/*
 * InsertManagerWidget.cpp
 *
 *  Created on: Jul 7, 2014
 *      Author: sk
 */

#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QGridLayout>
#include <QLineEdit>
#include <QLabel>
#include <QPushButton>
#include <QMessageBox>

#include "InsertManagerWidget.h"
#include "InsertManager.h"
#include "MeasurementFlightManager.h"
#include "Logger.h"
#include "String.h"

using namespace Utils::String;

namespace MFImport
{

InsertManagerWidget::InsertManagerWidget(QWidget* parent, Qt::WindowFlags f)
: QWidget (parent, f), current_mf_label_(0), current_tn_edit_(0), ds_box_(0)
{
    createGUIElements();
}

InsertManagerWidget::~InsertManagerWidget()
{
}

void InsertManagerWidget::createGUIElements ()
{
    QFont font_big;
    font_big.setPointSize(18);

    QFont font_bold;
    font_bold.setBold(true);

    QVBoxLayout *layout = new QVBoxLayout ();

    QLabel *label = new QLabel ("Management");
    label->setFont(font_big);
    layout->addWidget (label);

    QHBoxLayout *hlayout = new QHBoxLayout ();

    QGridLayout *glayout = new QGridLayout ();

    QLabel *mf_label = new QLabel ("Current Measurement Flight");
    glayout->addWidget (mf_label, 0, 0);

    current_mf_label_ = new QLabel (InsertManager::getInstance().getCurrentMeasurementFlight().c_str());
    glayout->addWidget (current_mf_label_, 0, 1);


    QLabel *tn_label = new QLabel ("Current Track Number");
    glayout->addWidget (tn_label, 1, 0);

    current_tn_edit_ = new QLineEdit (intToString(InsertManager::getInstance().getCurrentTrackNum()).c_str());
    current_tn_edit_->setMaximumWidth(500);
    glayout->addWidget (current_tn_edit_, 1, 1);
    connect (current_tn_edit_, SIGNAL(returnPressed()), this, SLOT(updateCurrentTrackNumber()));

    QLabel *ds_label = new QLabel ("Current Data Source");
    glayout->addWidget (ds_label, 2, 0);

    ds_box_ = new DataSourceComboBox (DBO_SYSTEM_TRACKS);
    if (InsertManager::getInstance().getCurrentDsId() == 0)
        InsertManager::getInstance().setCurrentDsId(ds_box_->getDataSource()); // assume the first value has been set
    else
        ds_box_->setDataSource(InsertManager::getInstance().getCurrentDsId());
    connect (ds_box_, SIGNAL(currentIndexChanged (const QString &)), this, SLOT(dsChanged (const QString &)));
    glayout->addWidget (ds_box_, 2, 1);

    hlayout->addLayout(glayout);

    QVBoxLayout *blayout = new QVBoxLayout ();

    QPushButton *find = new QPushButton ("Find Current Flight Matches");
    blayout->addWidget (find);
    connect (find, SIGNAL (clicked()), this, SLOT(getCurrentMFMatches()));

    QPushButton *correct = new QPushButton ("Correct Current Flight Height");
    blayout->addWidget (correct);
    connect (correct, SIGNAL (clicked()), this, SLOT(correctHeight()));

    QPushButton *insert = new QPushButton ("Insert Current Flight");
    blayout->addWidget (insert);
    connect (insert, SIGNAL (clicked()), this, SLOT(insertCurrentFlight()));

    blayout->addStretch();

    QPushButton *move = new QPushButton ("Move Current Track");
    blayout->addWidget (move);
    connect (move, SIGNAL (clicked()), this, SLOT(moveCurrentTrack()));

    QPushButton *del = new QPushButton ("Delete Current Track");
    blayout->addWidget (del);
    connect (del, SIGNAL (clicked()), this, SLOT(deleteCurrentTrack()));

    blayout->addStretch();

    hlayout->addLayout(blayout);

    layout->addLayout(hlayout);

    setLayout (layout);
}

void InsertManagerWidget::updateCurrentMeasurementFlight(std::string name)
{
    assert (current_mf_label_);

    if (name.size() == 0)
        current_mf_label_->setText ("");
    else
    {
        InsertManager::getInstance().setCurrentMeasurementFlight(name);
        current_mf_label_->setText (name.c_str());
    }
}

void InsertManagerWidget::updateCurrentTrackNumber ()
{
    assert (current_tn_edit_);

    unsigned int value = intFromString (current_tn_edit_->text().toStdString());
    InsertManager::getInstance().setCurrentTrackNum(value);
}

void InsertManagerWidget::moveCurrentTrack ()
{
    QMessageBox msgBox;
    msgBox.setWindowTitle("Move Current Track");
    std::string tmp = "Move "+intToString(InsertManager::getInstance().getCurrentTrackNum())+" to new Track Number?";
    msgBox.setText(tmp.c_str());
    msgBox.setStandardButtons(QMessageBox::Ok | QMessageBox::Cancel);
    msgBox.setDefaultButton(QMessageBox::Ok);
    int ret = msgBox.exec();

    if (ret == QMessageBox::Cancel)
        return;

    if (ret == QMessageBox::Ok)
    {
        unsigned int new_track_num = InsertManager::getInstance().moveCurrentTrack();

        QMessageBox msgBox;
        tmp = "Moved Track Number "+intToString (InsertManager::getInstance().getCurrentTrackNum())+" to "+intToString(new_track_num);
        msgBox.setText(tmp.c_str());
        msgBox.exec();
    }

}

void InsertManagerWidget::deleteCurrentTrack ()
{
    QMessageBox msgBox;
    msgBox.setWindowTitle("Delete Current Track");
    std::string tmp = "Delete Current Track "+intToString(InsertManager::getInstance().getCurrentTrackNum())+"?";
    msgBox.setText(tmp.c_str());
    msgBox.setStandardButtons(QMessageBox::Ok | QMessageBox::Cancel);
    msgBox.setDefaultButton(QMessageBox::Ok);
    int ret = msgBox.exec();

    if (ret == QMessageBox::Cancel)
        return;

    if (ret == QMessageBox::Ok)
    {
        InsertManager::getInstance().deleteCurrentTrack();

        QMessageBox msgBox;
        tmp = "Deleted Track Number "+intToString (InsertManager::getInstance().getCurrentTrackNum());
        msgBox.setText(tmp.c_str());
        msgBox.exec();
    }
}

void InsertManagerWidget::insertCurrentFlight ()
{
    QMessageBox msgBox;
    msgBox.setWindowTitle("Insert Measurement Flight");
    std::string tmp = "Insert Measurement Flight "+InsertManager::getInstance().getCurrentMeasurementFlight()+" as Track Number "
            +intToString(InsertManager::getInstance().getCurrentTrackNum())+"?";
    msgBox.setText(tmp.c_str());
    msgBox.setStandardButtons(QMessageBox::Ok | QMessageBox::Cancel);
    msgBox.setDefaultButton(QMessageBox::Ok);
    int ret = msgBox.exec();

    if (ret == QMessageBox::Cancel)
        return;

    if (ret == QMessageBox::Ok)
    {
        InsertManager::getInstance().insertCurrentMeasurementFlight();

        QMessageBox msgBox;
        tmp = "Inserted Measurement Flight as Track Number "+intToString (InsertManager::getInstance().getCurrentTrackNum());
        msgBox.setText(tmp.c_str());
        msgBox.exec();
    }


}

void InsertManagerWidget::dsChanged (const QString &)
{
    assert (ds_box_);
    InsertManager::getInstance().setCurrentDsId(ds_box_->getDataSource());
}

void InsertManagerWidget::getCurrentMFMatches ()
{
    std::map<unsigned int, std::pair <double, double> > matches = InsertManager::getInstance().getCurrentMFTrackMatches();

    QMessageBox msgBox;
    msgBox.setWindowTitle("Find Measurement Flight Matches");

    std::stringstream ss;

    std::map<unsigned int, std::pair <double, double> >::iterator it;
    for (it = matches.begin(); it != matches.end(); it++)
    {
        ss << "Match " << it->first << " Time [" << timeStringFromDouble(it->second.first) << " - "
                << timeStringFromDouble(it->second.second) << "]" << std::endl;
    }

    if (matches.size() == 0)
        ss << "No Matches found";
    else
        ss << std::endl << "Please enter the correct number as Current Track Number";


    msgBox.setText(ss.str().c_str());
    msgBox.setStandardButtons(QMessageBox::Ok);
    msgBox.setDefaultButton(QMessageBox::Ok);
    msgBox.exec();
}

void InsertManagerWidget::correctHeight ()
{
    QMessageBox msgBox;
    msgBox.setWindowTitle("Correct Measurement Flight Height");
    std::string tmp = "Correct Measurement Flight height"+InsertManager::getInstance().getCurrentMeasurementFlight()+"\n using Track Number "
            +intToString(InsertManager::getInstance().getCurrentTrackNum())+"?";
    msgBox.setText(tmp.c_str());
    msgBox.setStandardButtons(QMessageBox::Ok | QMessageBox::Cancel);
    msgBox.setDefaultButton(QMessageBox::Ok);
    int ret = msgBox.exec();

    if (ret == QMessageBox::Cancel)
        return;

    if (ret == QMessageBox::Ok)
    {
        try
        {
            MeasurementFlightManager::getInstance().correctHeight();
        } catch (std::exception &e)
        {
            QMessageBox msgBox;
            msgBox.setWindowTitle("Correct MF Height");
            std::string text = "Failed: Error '"+std::string(e.what())+"'";
            msgBox.setText(text.c_str());
            msgBox.setStandardButtons(QMessageBox::Cancel);
            msgBox.setDefaultButton(QMessageBox::Cancel);
            msgBox.exec();
            return;
        }

        QMessageBox msgBox;
        msgBox.setWindowTitle("Correct MF Height");
        std::string text = "Measurement Flight "+InsertManager::getInstance().getCurrentMeasurementFlight()+" height corrected\n"
                +" using Track "+intToString(InsertManager::getInstance().getCurrentTrackNum());
        msgBox.setText(text.c_str());
        msgBox.setStandardButtons(QMessageBox::Ok);
        msgBox.setDefaultButton(QMessageBox::Ok);
        msgBox.exec();
    }
}

} /* namespace MFImport */
