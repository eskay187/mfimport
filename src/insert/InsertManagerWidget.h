/*
 * InsertManagerWidget.h
 *
 *  Created on: Jul 7, 2014
 *      Author: sk
 */

#ifndef InsertManagerWidget_H_
#define InsertManagerWidget_H_

#include <QWidget>
#include <QComboBox>

#include "ATSDB.h"
#include "String.h"

class QLabel;
class QLineEdit;

namespace MFImport
{

class DataSourceComboBox : public QComboBox
{
    Q_OBJECT
public:
    DataSourceComboBox (DB_OBJECT_TYPE type, QWidget *parent = 0)
    : QComboBox (parent)
    {
        std::map<int, std::string> &sources = ATSDB::getInstance().getDataSources (type);

        std::map<int,std::string>::iterator it;

        for (it = sources.begin(); it != sources.end(); it++)
        {
            addItem((it->second+" ("+Utils::String::intToString(it->first)+")").c_str(), it->first);
        }
    }
    virtual ~DataSourceComboBox ()
    {

    }

    unsigned int getDataSource ()
    {
        bool ok;
        int data = itemData(currentIndex()).toInt(&ok);
        assert (ok);
        assert (data >= 0);
        return data;
    }

    void setDataSource (unsigned int ds_id)
    {
        int index = findData((int) ds_id);
        assert (index >= 0);
        setCurrentIndex(index);
    }
};

class InsertManagerWidget : public QWidget
{
    Q_OBJECT
public slots:
    void updateCurrentMeasurementFlight(std::string name); // name can be "" if none
    void updateCurrentTrackNumber ();
    void moveCurrentTrack ();
    void insertCurrentFlight ();
    void deleteCurrentTrack ();
    void dsChanged (const QString &);
    void getCurrentMFMatches ();
    void correctHeight ();
public:
    InsertManagerWidget(QWidget* parent = 0, Qt::WindowFlags f = 0);
    virtual ~InsertManagerWidget();

protected:
    QLabel *current_mf_label_;
    QLineEdit *current_tn_edit_;
    DataSourceComboBox *ds_box_;

    void createGUIElements ();
};

} /* namespace MFImport */

#endif /* InsertManagerWidget_H_ */
