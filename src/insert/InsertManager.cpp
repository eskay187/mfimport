/*
 * InsertManager.cpp
 *
 *  Created on: Jul 7, 2014
 *      Author: sk
 */

#include "InsertManager.h"
#include "ATSDB.h"
#include "DBObjectManager.h"
#include "String.h"
#include "DBOVariableSet.h"
#include "DBOVariable.h"
#include "Buffer.h"
#include "MeasurementFlightManager.h"
#include "MeasurementFlight.h"
#include "MeasurementFlightUpdate.h"
#include "Data.h"

using namespace Utils::Data;
using namespace Utils::String;

namespace MFImport
{

InsertManager::InsertManager()
: Singleton(), Configurable ("InsertManager", "InsertManager0", 0, "conf/config_insert.xml")
{
    registerParameter ("current_measurement_flight", &current_measurement_flight_, "");
    registerParameter ("current_track_num", &current_track_num_, 0);
    registerParameter ("current_ds_id", &current_ds_id_, 0);

    createSubConfigurables();
}

InsertManager::~InsertManager()
{
}

void InsertManager::generateSubConfigurable (std::string class_id, std::string instance_id)
{
    throw std::runtime_error ("InsertManager: generateSubConfigurable: unknown class_id "+class_id);
}

unsigned int InsertManager::moveCurrentTrack ()
{
    loginf << "InsertManager: moveCurrentTrack: moving track_num " << current_track_num_;

    assert (DBObjectManager::getInstance().existsDBOVariable(DBO_SYSTEM_TRACKS, "TRACK_NUM"));
    DBOVariable *track_num = DBObjectManager::getInstance().getDBOVariable(DBO_SYSTEM_TRACKS, "TRACK_NUM");

    std::string tn_min, tn_max;
    ATSDB::getInstance().getMinMaxOfVariable (track_num, "", tn_min, tn_max);
    loginf << "InsertManager: moveCurrentTrack: track num min " << tn_min << " max " << tn_max;

    unsigned int track_num_new = intFromString (tn_max);
    track_num_new++;

    loginf << "InsertManager: moveCurrentTrack: moving track " << track_num << " to " << track_num_new;
    ATSDB::getInstance().updateAllRowsWithVariableValue(track_num, intToString(current_track_num_), intToString(track_num_new));

    loginf << "InsertManager: moveCurrentTrack: done";

    return track_num_new;

}

void InsertManager::deleteCurrentTrack ()
{
    loginf << "InsertManager: deleteCurrentTrack: moving track_num " << current_track_num_;

    assert (DBObjectManager::getInstance().existsDBOVariable(DBO_SYSTEM_TRACKS, "TRACK_NUM"));
    DBOVariable *track_num = DBObjectManager::getInstance().getDBOVariable(DBO_SYSTEM_TRACKS, "TRACK_NUM");

    ATSDB::getInstance().deleteAllRowsWithVariableValue(track_num, intToString(current_track_num_));

    loginf << "InsertManager: deleteCurrentTrack: done";
}

void InsertManager::insertCurrentMeasurementFlight ()
{
    loginf << "InsertManager: insertCurrentMeasurementFlight: track_num " << current_track_num_ << " flight " << current_measurement_flight_;

    // an_rrt_samples hold standard dev a.s.o. for samples
    // an_rt tracks but no limits
    // an_rt_classifications aircraft classes
    // an_rt_mof_segments mode of flight segments
    // an_track_limits tod created end
    // an_tr_rt_associations
    // ds_tracker
    // le_ds

    assert (MeasurementFlightManager::getInstance().hasFlight(current_measurement_flight_));

    Buffer *buffer = getFlightBuffer ();
    assert (buffer);

    ATSDB::getInstance().insert(buffer, "sd_track");

    loginf << "InsertManager: insertCurrentMeasurementFlight: buffer inserted";

    delete buffer;

}

Buffer *InsertManager::getFlightBuffer () // code pleasing the flying spaghetti monster
{
    DBOVariableSet set; // to be added

    // sd_track
    // REC_NUM uint
    assert (DBObjectManager::getInstance().existsDBOVariable(DBO_SYSTEM_TRACKS, "REC_NUM"));
    DBOVariable *rec_num = DBObjectManager::getInstance().getDBOVariable(DBO_SYSTEM_TRACKS, "REC_NUM");
    set.add(rec_num);

    // REC_TIME_POSIX int
//    assert (DBObjectManager::getInstance().existsDBOVariable(DBO_SYSTEM_TRACKS, "REC_TIME_POSIX"));
//    DBOVariable *rec_time_posix = DBObjectManager::getInstance().getDBOVariable(DBO_SYSTEM_TRACKS, "REC_TIME_POSIX");

    // REC_TIME_MS int
//    assert (DBObjectManager::getInstance().existsDBOVariable(DBO_SYSTEM_TRACKS, "REC_TIME_MS"));
//    DBOVariable *rec_time_ms = DBObjectManager::getInstance().getDBOVariable(DBO_SYSTEM_TRACKS, "REC_TIME_MS");

    // DS_ID 27 int
    assert (DBObjectManager::getInstance().existsDBOVariable(DBO_SYSTEM_TRACKS, "DS_ID"));
    DBOVariable *ds_id = DBObjectManager::getInstance().getDBOVariable(DBO_SYSTEM_TRACKS, "DS_ID");
    set.add(ds_id);

    // REPORT_TYPE 2 char
//    assert (DBObjectManager::getInstance().existsDBOVariable(DBO_SYSTEM_TRACKS, "REPORT_TYPE"));
//    DBOVariable *report_type = DBObjectManager::getInstance().getDBOVariable(DBO_SYSTEM_TRACKS, "REPORT_TYPE");

    // TOD double // is int, other conversion
    assert (DBObjectManager::getInstance().existsDBOVariable(DBO_SYSTEM_TRACKS, "TOD"));
    DBOVariable *tod = DBObjectManager::getInstance().getDBOVariable(DBO_SYSTEM_TRACKS, "TOD");
    set.add(tod);

    // TOD_CALCULATED string 'N'
//    assert (DBObjectManager::getInstance().existsDBOVariable(DBO_SYSTEM_TRACKS, "TOD_CALCULATED"));
//    DBOVariable *tod_calculated = DBObjectManager::getInstance().getDBOVariable(DBO_SYSTEM_TRACKS, "TOD_CALCULATED");

    // DETECTION_TYPE char 1 psr,2 ssr, mode-s single roll call 5
//    assert (DBObjectManager::getInstance().existsDBOVariable(DBO_SYSTEM_TRACKS, "DETECTION_TYPE"));
//    DBOVariable *detection_type = DBObjectManager::getInstance().getDBOVariable(DBO_SYSTEM_TRACKS, "DETECTION_TYPE");

    // TARGET_ADDR int
    assert (DBObjectManager::getInstance().existsDBOVariable(DBO_SYSTEM_TRACKS, "TARGET_ADDR"));
    DBOVariable *target_addr = DBObjectManager::getInstance().getDBOVariable(DBO_SYSTEM_TRACKS, "TARGET_ADDR");
    set.add(target_addr);

    // CALLSIGN string
    assert (DBObjectManager::getInstance().existsDBOVariable(DBO_SYSTEM_TRACKS, "CALLSIGN"));
    DBOVariable *callsign = DBObjectManager::getInstance().getDBOVariable(DBO_SYSTEM_TRACKS, "CALLSIGN");
    set.add(callsign);

    // TRACK_NUM int
    assert (DBObjectManager::getInstance().existsDBOVariable(DBO_SYSTEM_TRACKS, "TRACK_NUM"));
    DBOVariable *track_num = DBObjectManager::getInstance().getDBOVariable(DBO_SYSTEM_TRACKS, "TRACK_NUM");
    set.add(track_num);

    // TRACK_CONFIRMED string 'Y'
//    assert (DBObjectManager::getInstance().existsDBOVariable(DBO_SYSTEM_TRACKS, "TRACK_CONFIRMED"));
//    DBOVariable *track_confirmed = DBObjectManager::getInstance().getDBOVariable(DBO_SYSTEM_TRACKS, "TRACK_CONFIRMED");

    // SIMULATED_TARGET string 'N'
//    assert (DBObjectManager::getInstance().existsDBOVariable(DBO_SYSTEM_TRACKS, "SIMULATED_TARGET"));
//    DBOVariable *simulated_target = DBObjectManager::getInstance().getDBOVariable(DBO_SYSTEM_TRACKS, "SIMULATED_TARGET");

    // AIRCRAFT_DERIVED string 'N'
//    assert (DBObjectManager::getInstance().existsDBOVariable(DBO_SYSTEM_TRACKS, "AIRCRAFT_DERIVED"));
//    DBOVariable *aircraft_derived = DBObjectManager::getInstance().getDBOVariable(DBO_SYSTEM_TRACKS, "AIRCRAFT_DERIVED");

    // TRACK_COASTED string 'N'
//    assert (DBObjectManager::getInstance().existsDBOVariable(DBO_SYSTEM_TRACKS, "TRACK_COASTED"));
//    DBOVariable *track_coasted = DBObjectManager::getInstance().getDBOVariable(DBO_SYSTEM_TRACKS, "TRACK_COASTED");

    // TRACK_END string 'Y','N'
    assert (DBObjectManager::getInstance().existsDBOVariable(DBO_SYSTEM_TRACKS, "TRACK_END"));
    DBOVariable *track_end = DBObjectManager::getInstance().getDBOVariable(DBO_SYSTEM_TRACKS, "TRACK_END");
    set.add(track_end);

    // TRACK_CREATED string 'Y','N'
    assert (DBObjectManager::getInstance().existsDBOVariable(DBO_SYSTEM_TRACKS, "TRACK_CREATED"));
    DBOVariable *track_created = DBObjectManager::getInstance().getDBOVariable(DBO_SYSTEM_TRACKS, "TRACK_CREATED");
    set.add(track_created);

    // TRACK_SLANT_CORR_TYPE char 0
//    assert (DBObjectManager::getInstance().existsDBOVariable(DBO_SYSTEM_TRACKS, "TRACK_SLANT_CORR_TYPE"));
//    DBOVariable *track_slant_range_corr_type = DBObjectManager::getInstance().getDBOVariable(DBO_SYSTEM_TRACKS, "TRACK_SLANT_CORR_TYPE");

    // FORMATION_FLIGHT string 'N'
//    assert (DBObjectManager::getInstance().existsDBOVariable(DBO_SYSTEM_TRACKS, "FORMATION_FLIGHT"));
//    DBOVariable *formation_flight = DBObjectManager::getInstance().getDBOVariable(DBO_SYSTEM_TRACKS, "FORMATION_FLIGHT");

    // TRACK_AMALGAMATED string 'N'
//    assert (DBObjectManager::getInstance().existsDBOVariable(DBO_SYSTEM_TRACKS, "TRACK_AMALGAMATED"));
//    DBOVariable *track_amalgamated = DBObjectManager::getInstance().getDBOVariable(DBO_SYSTEM_TRACKS, "TRACK_AMALGAMATED");

    // SPI string 'N', 'Y', NULL

    // MIL_EMERGENCY string 'N'
//    assert (DBObjectManager::getInstance().existsDBOVariable(DBO_SYSTEM_TRACKS, "MIL_EMERGENCY"));
//    DBOVariable *mil_emergency = DBObjectManager::getInstance().getDBOVariable(DBO_SYSTEM_TRACKS, "MIL_EMERGENCY");

    // TRANSPONDER_DELAY_CORR string 'Y'
//    assert (DBObjectManager::getInstance().existsDBOVariable(DBO_SYSTEM_TRACKS, "TRANSPONDER_DELAY_CORR"));
//    DBOVariable *transponder_delay_corr = DBObjectManager::getInstance().getDBOVariable(DBO_SYSTEM_TRACKS, "TRANSPONDER_DELAY_CORR");

    // POS_LOCAL_X_NM double x
    // POS_LOCAL_Y_NM double y

    // POS_LAT_DEG double x
    assert (DBObjectManager::getInstance().existsDBOVariable(DBO_SYSTEM_TRACKS, "POS_LAT_DEG"));
    DBOVariable *pos_lat_deg = DBObjectManager::getInstance().getDBOVariable(DBO_SYSTEM_TRACKS, "POS_LAT_DEG");
    set.add(pos_lat_deg);

    // POS_LONG_DEG double y
    assert (DBObjectManager::getInstance().existsDBOVariable(DBO_SYSTEM_TRACKS, "POS_LONG_DEG"));
    DBOVariable *pos_long_deg = DBObjectManager::getInstance().getDBOVariable(DBO_SYSTEM_TRACKS, "POS_LONG_DEG");
    set.add(pos_long_deg);

    // ALT_BARO_FT int z
    assert (DBObjectManager::getInstance().existsDBOVariable(DBO_SYSTEM_TRACKS, "ALT_BARO_FT"));
    DBOVariable *alt_baro_ft = DBObjectManager::getInstance().getDBOVariable(DBO_SYSTEM_TRACKS, "ALT_BARO_FT");
    set.add(alt_baro_ft);

    // MODE3A_V string 'Y'
    assert (DBObjectManager::getInstance().existsDBOVariable(DBO_SYSTEM_TRACKS, "MODE3A_V"));
    DBOVariable *mode3a_v = DBObjectManager::getInstance().getDBOVariable(DBO_SYSTEM_TRACKS, "MODE3A_V");
    set.add(mode3a_v);

    // MODE3A_G string 'N'
    assert (DBObjectManager::getInstance().existsDBOVariable(DBO_SYSTEM_TRACKS, "MODE3A_G"));
    DBOVariable *mode3a_g = DBObjectManager::getInstance().getDBOVariable(DBO_SYSTEM_TRACKS, "MODE3A_G");
    set.add(mode3a_g);

    // MODE3A_CODE int
    assert (DBObjectManager::getInstance().existsDBOVariable(DBO_SYSTEM_TRACKS, "MODE3A_CODE"));
    DBOVariable *mode3a_code = DBObjectManager::getInstance().getDBOVariable(DBO_SYSTEM_TRACKS, "MODE3A_CODE");
    set.add(mode3a_code);

    // MODEC_V string 'Y'
    assert (DBObjectManager::getInstance().existsDBOVariable(DBO_SYSTEM_TRACKS, "MODEC_V"));
    DBOVariable *modec_v = DBObjectManager::getInstance().getDBOVariable(DBO_SYSTEM_TRACKS, "MODEC_V");
    set.add(modec_v);

    // MODEC_G string 'N'
    assert (DBObjectManager::getInstance().existsDBOVariable(DBO_SYSTEM_TRACKS, "MODEC_G"));
    DBOVariable *modec_g = DBObjectManager::getInstance().getDBOVariable(DBO_SYSTEM_TRACKS, "MODEC_G");
    set.add(modec_g);

    // MODEC_CODE_FT int z
    assert (DBObjectManager::getInstance().existsDBOVariable(DBO_SYSTEM_TRACKS, "MODEC_CODE_FT"));
    DBOVariable *modec_code_ft = DBObjectManager::getInstance().getDBOVariable(DBO_SYSTEM_TRACKS, "MODEC_CODE_FT");
    set.add(modec_code_ft);

    // GOUNDSPEED_KT double spd
    // HEADING_DEG double hd
    // MOF_TRANS char 0,1,2,3
    // MOF_LONG char 0,1,2,3
    // MOF_VERT char 0,1,2,3
    // CALC_VERTICAL_RATE_FTM double ?
    // DOM_POSIX int 0
    // CIVIL_EMERGENCY char 0,4,5,NULL

    PropertyList list = set.getPropertyList (DBO_SYSTEM_TRACKS);

    unsigned int rec_num_ind = list.getPropertyIndex(rec_num->getCurrentVariableName());
    unsigned int ds_id_ind = list.getPropertyIndex(ds_id->getCurrentVariableName());
    unsigned int tod_ind = list.getPropertyIndex(tod->getCurrentVariableName());
    unsigned int target_addr_ind = list.getPropertyIndex(target_addr->getCurrentVariableName());
    unsigned int callsign_ind = list.getPropertyIndex(callsign->getCurrentVariableName());
    unsigned int track_num_ind = list.getPropertyIndex(track_num->getCurrentVariableName());
    unsigned int track_end_ind = list.getPropertyIndex(track_end->getCurrentVariableName());
    unsigned int track_created_ind = list.getPropertyIndex(track_created->getCurrentVariableName());
    unsigned int pos_lat_deg_ind = list.getPropertyIndex(pos_lat_deg->getCurrentVariableName());
    unsigned int pos_long_deg_ind = list.getPropertyIndex(pos_long_deg->getCurrentVariableName());
    unsigned int alt_baro_ft_ind = list.getPropertyIndex(alt_baro_ft->getCurrentVariableName());
    unsigned int mode3a_v_ind = list.getPropertyIndex(mode3a_v->getCurrentVariableName());
    unsigned int mode3a_g_ind = list.getPropertyIndex(mode3a_g->getCurrentVariableName());
    unsigned int mode3a_code_ind = list.getPropertyIndex(mode3a_code->getCurrentVariableName());
    unsigned int modec_v_ind = list.getPropertyIndex(modec_v->getCurrentVariableName());
    unsigned int modec_g_ind = list.getPropertyIndex(modec_g->getCurrentVariableName());
    unsigned int modec_code_ft_ind = list.getPropertyIndex(modec_code_ft->getCurrentVariableName());

    std::string rn_min, rn_max;
    ATSDB::getInstance().getMinMaxOfVariable (rec_num, "", rn_min, rn_max);
    loginf << "InsertManager: insertCurrentMeasurementFlight: rec num min " << rn_min << " max " << rn_max;
    unsigned int rec_num_cnt = intFromString (rn_max);
    rec_num_cnt++;

    Buffer *buffer = new Buffer (list, DBO_SYSTEM_TRACKS);

    assert (MeasurementFlightManager::getInstance().hasFlight(current_measurement_flight_));
    MeasurementFlight *flight = MeasurementFlightManager::getInstance().getFlight (current_measurement_flight_);

    const std::map <double, MeasurementFlightUpdate*> &updates = flight->getUpdates ();
    std::map <double, MeasurementFlightUpdate*>::const_iterator it;
    std::vector <void*>* addresses=0;

    double value_tod;
    double value_latitude;
    double value_longitude;
    double value_altitude;
    bool value_has_mode_a_code;
    bool value_has_target_address;
    bool value_has_target_identification;

    unsigned int cnt=0;
    unsigned int last_cnt = updates.size()-1;
    MeasurementFlightUpdate *update=0;

    for (it = updates.begin(); it != updates.end(); it++)
    {
        update = it->second;

        value_tod = update->getTod();
        value_latitude = update->getLatitude();
        value_longitude = update->getLongitude();
        value_altitude = update->getAltitude();
        value_has_mode_a_code = update->hasModeACode();
        value_has_target_address = update->hasTargetAddress();
        value_has_target_identification = update->hasTargetIdentification();

        if (cnt != 0)
            buffer->incrementIndex();

        addresses = buffer->getAdresses();

         *(int*) addresses->at(rec_num_ind) = rec_num_cnt;
         *(int*) addresses->at(ds_id_ind) = current_ds_id_;
         *(double*) addresses->at(tod_ind) = 128.0 * value_tod; // should be int, v7 time unit

         if (value_has_target_address)
             *(int*) addresses->at(target_addr_ind) = update->getTargetAddress();
         else
             setNan(target_addr->getDataType(), addresses->at(target_addr_ind));

         if (value_has_target_identification)
             *(std::string*) addresses->at(callsign_ind) = update->getTargetIdentification();
         else
             setNan(callsign->getDataType(), addresses->at(callsign_ind));

         *(int*) addresses->at(track_num_ind) = current_track_num_;

         if (cnt == last_cnt)
             *(std::string*) addresses->at(track_end_ind) = "Y";
         else
             *(std::string*) addresses->at(track_end_ind) = "N";

         if (cnt == 0)
             *(std::string*) addresses->at(track_created_ind) = "Y";
         else
             *(std::string*) addresses->at(track_created_ind) = "N";

         *(double*) addresses->at(pos_lat_deg_ind) = value_latitude;
         *(double*) addresses->at(pos_long_deg_ind) = value_longitude;
         *(int*) addresses->at(alt_baro_ft_ind) = value_altitude;
         *(std::string*) addresses->at(mode3a_v_ind) = "Y";
         *(std::string*) addresses->at(mode3a_g_ind) = "N";

         if (value_has_mode_a_code)
             *(int*)addresses->at(mode3a_code_ind) = update->getModeACode();
         else
             setNan(mode3a_code->getDataType(), addresses->at(mode3a_code_ind));

         *(std::string*) addresses->at(modec_v_ind) = "Y";
         *(std::string*) addresses->at(modec_g_ind) = "N";
         *(int*) addresses->at(modec_code_ft_ind) = value_altitude;

         rec_num_cnt++;
         cnt++;
    }

    loginf << "InsertManager: getFlightBuffer: buffer with " << buffer->getSize() << " rows created";

    return buffer;
}

std::map<unsigned int, std::pair <double, double> > InsertManager::getCurrentMFTrackMatches ()
{
    std::map<unsigned int, std::pair <double, double> > matches;

    assert (MeasurementFlightManager::getInstance().hasFlight(current_measurement_flight_));

    MeasurementFlight *flight = MeasurementFlightManager::getInstance().getFlight(current_measurement_flight_);

    bool has_mode_a = flight->getModeACodes().size() == 1;
    unsigned int mode_a=0;

    if (has_mode_a)
        mode_a = *(flight->getModeACodes().begin());

    bool has_ta = flight->getTargetAddresses().size() == 1;
    unsigned int ta = 0;

    if (has_ta)
        ta= *(flight->getTargetAddresses().begin());

    bool has_ti = flight->getTargetIdentifications().size() == 1;
    std::string ti;
    if (has_ti)
        ti = *(flight->getTargetIdentifications().begin());

    loginf << "InsertManager: getCurrentMFTrackMatches: has ma" << has_mode_a << " ma " << mode_a
            << " has ta " << has_ta << " ta " << ta << " has ti " << has_ti << " ti " << ti;

    Buffer *result = ATSDB::getInstance().getTrackMatches (has_mode_a, mode_a, has_ta, ta, has_ti, ti);

    if (result->getFirstWrite())
        return matches;

    result->setIndex(0);
    std::vector <void*> *addresses = 0;

    unsigned int track_num;
    double tod_min;
    double tod_max;

    for (unsigned int cnt=0; cnt < result->getSize(); cnt++)
    {
        if (cnt != 0)
            result->incrementIndex();

        addresses = result->getAdresses();

        track_num = *(int*) addresses->at(0);
        tod_min = *(double*) addresses->at(1) / 128.0;
        tod_max = *(double*) addresses->at(2) / 128.0;

        loginf << "InsertManager: getCurrentMFTrackMatches: match " << track_num << " [" << timeStringFromDouble(tod_min)
                << " - " << timeStringFromDouble(tod_max) << "]";

        matches[track_num] = std::make_pair(tod_min, tod_max);
    }

    delete result;

    return matches;
}

} /* namespace MFImport */
