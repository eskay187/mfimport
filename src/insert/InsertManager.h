/*
 * InsertManager.h
 *
 *  Created on: Jul 7, 2014
 *      Author: sk
 */

#ifndef INSERTMANAGER_H_
#define INSERTMANAGER_H_

#include "Singleton.h"
#include "Configurable.h"
#include "MeasurementFlightManager.h"

#include "Buffer.h"

namespace MFImport
{

class InsertManager : public Singleton, public Configurable
{
public:
    static InsertManager& getInstance()
    {
      static InsertManager instance;
      return instance;
    }

    virtual ~InsertManager();

    virtual void generateSubConfigurable (std::string class_id, std::string instance_id);

    const std::string& getCurrentMeasurementFlight() const
    {
        return current_measurement_flight_;
    }

    void setCurrentMeasurementFlight(const std::string& name)
    {
        assert (MeasurementFlightManager::getInstance().hasFlight(name));
        current_measurement_flight_ = name;
    }

    unsigned int getCurrentTrackNum() const
    {
        return current_track_num_;
    }

    void setCurrentTrackNum(unsigned int value)
    {
        current_track_num_ = value;
    }

    unsigned int moveCurrentTrack ();
    void deleteCurrentTrack ();
    void insertCurrentMeasurementFlight ();

    unsigned int getCurrentDsId() const { return current_ds_id_; }

    void setCurrentDsId(unsigned int dsId) { current_ds_id_ = dsId; }

    std::map<unsigned int, std::pair <double, double> > getCurrentMFTrackMatches ();

protected:
    std::string current_measurement_flight_;
    unsigned int current_track_num_;
    unsigned int current_ds_id_;

    InsertManager();

    virtual void checkSubConfigurables () {}
    Buffer *getFlightBuffer ();
};

} /* namespace MFImport */

#endif /* INSERTMANAGER_H_ */
