/*
 * MeasurementFlightManagerWidget.h
 *
 *  Created on: Jul 3, 2014
 *      Author: sk
 */

#ifndef MEASUREMENTFLIGHTMANAGERWIDGET_H_
#define MEASUREMENTFLIGHTMANAGERWIDGET_H_

#include <QWidget>
#include <map>

class QLineEdit;
class QTableWidget;
class QTableWidgetItem;
class QPushButton;

namespace MFImport
{
class GPSTrail;

class MeasurementFlightManagerWidget : public QWidget
{
    Q_OBJECT
public slots:
    void nameChanged ();
    void deleteFlight ();
    void insertFlight ();
    void convert (GPSTrail *trail);

signals:
    void insertFlight (std::string name);

public:
    MeasurementFlightManagerWidget(QWidget* parent = 0, Qt::WindowFlags f = 0);
    virtual ~MeasurementFlightManagerWidget();

protected:
    QTableWidget *table_;

    std::map <QLineEdit *, std::string> name_items_;
    std::map <QPushButton *, std::string> delete_buttons_;
    std::map <QPushButton *, std::string> insert_buttons_;

    void createGUIElements ();

    void updateFlightTable ();
};

} /* namespace MFImport */

#endif /* MEASUREMENTFLIGHTMANAGERWIDGET_H_ */
