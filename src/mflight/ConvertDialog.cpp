/*
 * ConvertDialog.cpp
 *
 *  Created on: Jul 3, 2014
 *      Author: sk
 */

#include "ConvertDialog.h"
#include "Logger.h"
#include "String.h"
#include "MeasurementFlightManager.h"

#include <QVBoxLayout>
#include <QGridLayout>
#include <QLabel>
#include <QCheckBox>
#include <QLineEdit>
#include <QPushButton>
#include <QMessageBox>

using namespace Utils::String;

namespace MFImport
{

ConvertDialog::ConvertDialog(std::string name, QWidget *parent)
: QDialog (parent), name_(name), name_edit_(0), mode_a_check_(0), mode_a_code_edit_(0), target_address_check_(0), target_address_edit_(0),
 target_identification_check_(0), target_identification_edit_(0), accepted_(false)

{
    setMinimumWidth(800);

    createGUIElements();
}

ConvertDialog::~ConvertDialog()
{
}

void ConvertDialog::createGUIElements ()
{
    QFont font_bold;
    font_bold.setBold(true);

    QFont font_big;
    font_big.setPointSize(18);

    QVBoxLayout *layout = new QVBoxLayout ();

    QLabel *label = new QLabel ("Convert GPS Trail to Measurement Flight");
    label->setFont (font_big);
    layout->addWidget (label);

    QGridLayout *glayout = new QGridLayout ();

    QLabel *name_label = new QLabel ("Measurement Flight Name");
    glayout->addWidget (name_label, 0, 0);

    name_edit_ = new QLineEdit ();
    name_edit_->setText (name_.c_str());
    glayout->addWidget (name_edit_, 0, 1);

    mode_a_check_ = new QCheckBox ("Mode A Code (octal, e.g. 7777)");
    glayout->addWidget (mode_a_check_, 1, 0);

    mode_a_code_edit_ = new QLineEdit();
    glayout->addWidget (mode_a_code_edit_, 1, 1);

    target_address_check_ = new QCheckBox("Target Address (hex, e.g. AABBCC)");
    glayout->addWidget (target_address_check_, 2, 0);

    target_address_edit_ = new QLineEdit ();
    glayout->addWidget (target_address_edit_, 2, 1);

    target_identification_check_ = new QCheckBox ("Target Identification");
    glayout->addWidget (target_identification_check_, 3, 0);

    target_identification_edit_ = new QLineEdit ();
    glayout->addWidget (target_identification_edit_, 3, 1);

    layout->addLayout (glayout);

    QHBoxLayout *blayout = new QHBoxLayout ();

    QPushButton *cancel = new QPushButton ("Cancel");
    connect (cancel, SIGNAL(clicked()), this, SLOT(cancel()));
    blayout->addWidget (cancel);

    blayout->addStretch();

    QPushButton *ok = new QPushButton ("OK");
    connect (ok, SIGNAL(clicked()), this, SLOT(ok()));
    blayout->addWidget (ok);

    layout->addLayout (blayout);

    setLayout (layout);
}

void ConvertDialog::ok ()
{
    loginf << "ConvertDialog: ok";

    assert (name_edit_);
    assert (mode_a_check_);
    assert (mode_a_code_edit_);
    assert (target_address_check_);
    assert (target_address_edit_);
    assert (target_identification_check_);
    assert (target_identification_edit_);

    if (name_edit_->text().size() == 0)
    {
        QMessageBox::warning ( this, "Measurement Flight Convert", "Name length "+tr(intToString(name_edit_->text().size()).c_str())+" not allowed");
        return;
    }

    if (MeasurementFlightManager::getInstance().hasFlight(name_edit_->text().toStdString()))
    {
        QMessageBox::warning ( this, "Measurement Flight Convert", "Name "+name_edit_->text()+" already exists");
        return;
    }

    if (mode_a_check_->checkState() == Qt::Checked)
    {
        if (mode_a_code_edit_->text().size() != 4)
        {
            QMessageBox::warning ( this, "Measurement Flight Convert", "Mode 3/A Code length "
                    +tr(intToString(mode_a_code_edit_->text().size()).c_str())+" not allowed, must be 4");
            return;
        }

        bool ok;
        int m3a = intFromOctalString(mode_a_code_edit_->text().toStdString(), &ok);

        if (!ok)
        {
            QMessageBox::warning ( this, "Measurement Flight Convert", "Mode 3/A Code "+mode_a_code_edit_->text()+" could not be read");
            return;
        }

        if (m3a < 0 || m3a > 4095)
        {
            QMessageBox::warning ( this, "Measurement Flight Convert", "Mode 3/A Code "+tr(intToString(m3a).c_str())+" forbidden");
            return;
        }
    }

    if (target_address_check_->checkState() == Qt::Checked)
    {
        if (target_address_edit_->text().size() != 6)
        {
            QMessageBox::warning ( this, "Measurement Flight Convert", "Target Address length "
                    +tr(intToString(target_address_edit_->text().size()).c_str())+" not allowed, must be 6");
            return;
        }

        bool ok;
        intFromHexString(target_address_edit_->text().toStdString(), &ok);

        if (!ok)
        {
            QMessageBox::warning ( this, "Measurement Flight Convert", "Target Address "+target_address_edit_->text()+" could not be read");
            return;
        }
    }

    if (target_identification_check_->checkState() == Qt::Checked)
    {
        if (target_identification_edit_->text().size() == 0 || target_identification_edit_->text().size() > 8)
        {
            QMessageBox::warning ( this, "Measurement Flight Convert", "Target Identification length "
                    +tr(intToString(target_identification_edit_->text().size()).c_str())+" not allowed, must be 1-8");
            return;
        }
    }

    accepted_=true;
    accept();
}
void ConvertDialog::cancel ()
{
    loginf << "ConvertDialog: cancel";

    reject ();
}

std::string ConvertDialog::getName ()
{
    assert (accepted_);
    assert (name_edit_);
    return name_edit_->text().toStdString();
}

bool ConvertDialog::hasModeA ()
{
    assert (accepted_);
    assert (mode_a_check_);
    return mode_a_check_->checkState() == Qt::Checked;

}
unsigned int ConvertDialog::getModeA ()
{
    assert (accepted_);
    assert (mode_a_code_edit_);
    assert (hasModeA());
    bool ok;
    unsigned int m3a = intFromOctalString(mode_a_code_edit_->text().toStdString(), &ok);
    assert (ok);

    return m3a;
}
bool ConvertDialog::hasTargetAddress ()
{
    assert (accepted_);
    assert (target_address_check_);
    return target_address_check_->checkState() == Qt::Checked;

}
unsigned int ConvertDialog::getTargetAddress ()
{
    assert (accepted_);
    assert (target_address_edit_);
    assert (hasTargetAddress());
    bool ok;
    unsigned int ta= intFromHexString(target_address_edit_->text().toStdString(), &ok);
    assert (ok);
    return ta;
}
bool ConvertDialog::hasTargetIdentification ()
{
    assert (accepted_);
    assert (target_identification_check_);
    return target_identification_check_->checkState() == Qt::Checked;
}
std::string ConvertDialog::getTargetIdentification ()
{
    assert (accepted_);
    assert (target_identification_edit_);
    assert (hasTargetIdentification());
    return target_identification_edit_->text().toStdString();
}


} /* namespace MFImport */
