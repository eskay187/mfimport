/*
 * MeasurementFlight.cpp
 *
 *  Created on: Jun 24, 2014
 *      Author: sk
 */

#include "GPSTrail.h"
#include "GPSUpdate.h"
#include "MeasurementFlight.h"
#include "MeasurementFlightUpdate.h"
#include "String.h"

#include <algorithm>
#include <iterator>

using namespace Utils::String;

namespace MFImport
{
MeasurementFlight::MeasurementFlight(std::string class_id, std::string instance_id, Configurable *parent)
: Configurable (class_id, instance_id, parent), tod_min_(0), tod_max_(0)
{
    registerParameter ("name", &name_, "");
    assert (name_.size() > 0);

    createSubConfigurables();

}

MeasurementFlight::~MeasurementFlight()
{
    std::map <double, MeasurementFlightUpdate*>::iterator it;

    for (it = updates_.begin(); it != updates_.end(); it++)
        delete it->second;
    updates_.clear();
}

void MeasurementFlight::generateSubConfigurable (std::string class_id, std::string instance_id)
{
    if (class_id == "MeasurementFlightUpdate")
    {
        MeasurementFlightUpdate *update = new MeasurementFlightUpdate (class_id, instance_id, this);

        if (updates_.find(update->getTod()) != updates_.end())
        {
            logwrn << "MeasurementFlightUpdate: generateSubConfigurable: removing new update with same tod as another "
                    << timeStringFromDouble(update->getTod());
            delete update;
            return;
        }

        if (updates_.size() == 0)
        {
            tod_min_ = update->getTod();
            tod_max_ = update->getTod();
        }
        else
        {
            if (update->getTod() < tod_min_)
                tod_min_ = update->getTod();

            if (update->getTod() > tod_max_)
                tod_max_ = update->getTod();
        }

        if (update->hasModeACode())
            if (mode_a_codes_.find(update->getModeACode()) == mode_a_codes_.end())
                mode_a_codes_.insert(update->getModeACode());

        if (update->hasTargetAddress())
            if (target_addresses_.find(update->getTargetAddress()) == target_addresses_.end())
                target_addresses_.insert(update->getTargetAddress());

        if (update->hasTargetIdentification())
            if (target_identificatons_.find(update->getTargetIdentification()) == target_identificatons_.end())
                target_identificatons_.insert(update->getTargetIdentification());

        updates_[update->getTod()] = update;
    }
    else
        throw std::runtime_error ("MeasurementFlightUpdate: generateSubConfigurable: unknown class_id "+class_id);
}

void MeasurementFlight::importGPSTrail (GPSTrail *trail, bool has_mode_a, unsigned int mode_a, bool has_ta, unsigned int ta,
        bool has_ti, std::string ti)
{
    assert (updates_.size() == 0);

    loginf << "MeasurementFlight: importGPSTrail: start";

    const std::map <double, GPSUpdate*> &updates = trail->getUpdates();
    std::map <double, GPSUpdate*>::const_iterator it;

    const GPSUpdate *current;
    for (it = updates.begin(); it != updates.end(); it++)
    {
        current = it->second;

        Configuration &config = addNewSubConfiguration("MeasurementFlightUpdate", "MeasurementFlightUpdate"+Utils::String::intToString(updates_.size()));
        config.addParameterDouble("tod", current->getTod());
        config.addParameterDouble("latitude", current->getLatitude());
        config.addParameterDouble("longitude", current->getLongitude());
        config.addParameterDouble("altitude", current->getAltitude());
        config.addParameterBool ("has_mode_a_code", has_mode_a);
        config.addParameterUnsignedInt ("mode_a_code", mode_a);
        config.addParameterBool ("has_target_address", has_ta);
        config.addParameterUnsignedInt ("target_address", ta);
        config.addParameterBool ("has_target_identification", has_ti);
        config.addParameterString ("target_identification", ti);
        generateSubConfigurable (config.getClassId(), config.getInstanceId());
    }

    loginf << "MeasurementFlight: importGPSTrail: imported " << updates_.size() << " updates";
}

std::string MeasurementFlight::getModeACodesAsString ()
{
    std::stringstream ss;

    std::set<unsigned int>::iterator it;
    for (it = mode_a_codes_.begin(); it != mode_a_codes_.end(); it++)
    {
        if (it != mode_a_codes_.begin())
            ss << ", ";

        ss << octStringFromInt(*it, 4, '0');
    }

    return ss.str();
}
std::string MeasurementFlight::getTargetAddressesAsString ()
{
    std::stringstream ss;

    std::set<unsigned int>::iterator it;
    for (it = target_addresses_.begin(); it != target_addresses_.end(); it++)
    {
        if (it != target_addresses_.begin())
            ss << ", ";

        ss << hexStringFromInt(*it, 6, '0');
    }

    return ss.str();
}
std::string MeasurementFlight::getTargetIdentificationsAsString ()
{
    std::stringstream ss;

    std::set<std::string>::iterator it;
    for (it = target_identificatons_.begin(); it != target_identificatons_.end(); it++)
    {
        if (it != target_identificatons_.begin())
            ss << ", ";

        ss << *it;
    }

    return ss.str();
}

//void MeasurementFlight::correctHeights (const std::map <double, double> &height_data)
//{
//    loginf << "MeasurementFlight: correctHeights";
//
//    std::map <double, MeasurementFlightUpdate*>::iterator it;
//
//    MeasurementFlightUpdate *current;
//
//    bool first_success=true;
//    bool success=false;
//    double baro_altitude_calculated=0.0;
//
//    std::vector <double> baro_data;
//    std::vector <double> geo_data;
//
//    double a=0;
//    double b=1;
//
//    double a_new;
//    double b_new;
//
//    //assert (updates_.size() > 5);
//
//    for (it = updates_.begin(); it != updates_.end(); it++)
//    {
//        current = it->second;
//
//        baro_altitude_calculated = getHeightAtTime (height_data, current->getTod(), success);
//
//        if (!success)
//        {
//            if (!first_success)
//            {
//                loginf << "MeasurementFlight: correctHeights: a " << a << " b " << b << " geo height corrected " <<  (current->getAltitude()-a)/b
//                        << " org " << current->getAltitude();
//                current->setAltitude((current->getAltitude()-a)/b);
//            }
//            continue;
//        }
//
//        //loginf << "success got baro calc " << baro_altitude_calculated << " geo " << current->getAltitude();
//
//        assert (baro_data.size() == geo_data.size());
//
//        // success
//        baro_data.push_back (baro_altitude_calculated);
//        geo_data.push_back (current->getAltitude());
//
//        if (baro_data.size() <= 500)
//            continue;
//
//        baro_data.erase (baro_data.begin());  // remove first element
//        geo_data.erase (geo_data.begin());
//
//        getCorrectionParametersMLSQ (baro_data, geo_data, a_new, b_new);
//
//        if (b_new > 0.8)
//        {
//            a=a_new;
//            b=b_new;
//        }
//
//        if (first_success)
//        {
//            MeasurementFlightUpdate *current_again=0;
//            std::map <double, MeasurementFlightUpdate*>::iterator it_again;
//
//            for (it_again = updates_.begin(); it_again != it; it_again++)
//            {
//                current_again = it_again->second;
//                loginf << "MeasurementFlight: correctHeights: a " << a << " b " << b << " past geo height corrected " <<  (current_again->getAltitude()-a)/b
//                        << " org " << current_again->getAltitude() << " tod " << timeStringFromDouble(current_again->getTod());
//                current_again->setAltitude((current_again->getAltitude()-a)/b);
//            }
//            first_success=false;
//        }
//
//        loginf << "MeasurementFlight: correctHeights: a " << a << " b " << b << " geo height corrected "
//                <<  (current->getAltitude()-a)/b << " rrt " << baro_altitude_calculated << " tod " << timeStringFromDouble(current->getTod());
//
//        current->setAltitude((current->getAltitude()-a)/b);
//    }
//
//    loginf << "MeasurementFlight: correctHeights: done";
//}

void MeasurementFlight::correctHeights (const std::map <double, double> &height_data)
{
    loginf << "MeasurementFlight: correctHeights";

    std::map <double, MeasurementFlightUpdate*>::iterator it;

    MeasurementFlightUpdate *current;

    bool success=false;
    double baro_altitude_calculated=0.0;


    //assert (updates_.size() > 5);

    std::map <double, double> baro_altitudes_calculated;
    std::map <double, double> geo_altitudes;

    for (it = updates_.begin(); it != updates_.end(); it++)
    {
        current = it->second;
        baro_altitude_calculated = getHeightAtTime (height_data, current->getTod(), success);

        if (success)
        {
            baro_altitudes_calculated[current->getTod()] = baro_altitude_calculated;
            geo_altitudes[current->getTod()] = current->getAltitude();
        }
    }

    std::vector <double> baro_data;
    std::vector <double> geo_data;

    double a=0;
    double b=1;

//    double a_new=0;
//    double b_new=1;


    std::map <double, double>::const_iterator baro_before_it;
    std::map <double, double>::const_iterator baro_after_it;
//    std::map <double, double>::const_iterator baro_current_it;
    std::map <double, double>::const_iterator baro_it;

    std::map <double, double>::const_iterator geo_before_it;
    std::map <double, double>::const_iterator geo_after_it;
//    std::map <double, double>::const_iterator geo_current_it;
    std::map <double, double>::const_iterator geo_it;

    for (it = updates_.begin(); it != updates_.end(); it++)
    {
        current = it->second;

//        loginf << "1";

//        baro_current_it = baro_altitudes_calculated.find(current->getTod());
//        if (baro_current_it == baro_altitudes_calculated.end())
//            baro_current_it = baro_altitudes_calculated.lower_bound(current->getTod());
//        assert (baro_current_it != baro_altitudes_calculated.end());

        baro_before_it = baro_altitudes_calculated.lower_bound(current->getTod()-120);
        baro_after_it = baro_altitudes_calculated.upper_bound(current->getTod()+120);

//        loginf << "2";
//
//        baro_before_it = baro_current_it;
//        std::advance (baro_before_it, -15);
//
//        baro_after_it = baro_current_it;
//        std::advance (baro_after_it, 15);
//
//        loginf << "3";
//
        baro_data.clear();
        for (baro_it = baro_before_it; baro_it != baro_after_it; baro_it++)
            baro_data.push_back(baro_it->second);
//
//        loginf << "4";
//
//        geo_current_it = geo_altitudes.find(current->getTod());
//        if (geo_current_it == geo_altitudes.end())
//            geo_current_it = geo_altitudes.lower_bound(current->getTod());
//        assert (geo_current_it != geo_altitudes.end());
//
//        loginf << "5";

        geo_before_it = geo_altitudes.lower_bound(current->getTod()-120);
        geo_after_it = geo_altitudes.upper_bound(current->getTod()+120);

//        geo_before_it = geo_current_it;
//        std::advance (geo_before_it, -15);
//
//        geo_after_it = geo_current_it;
//        std::advance (geo_after_it, 15);
//
//        loginf << "6";

        geo_data.clear();
        for (geo_it = geo_before_it; geo_it != geo_after_it; geo_it++)
            geo_data.push_back(geo_it->second);

//        loginf << "7";

        assert (baro_data.size() == geo_data.size());

        if (baro_data.size() != 0)
            getCorrectionParametersMLSQ (baro_data, geo_data, a, b);
        else
            logwrn << "MeasurementFlight: correctHeights: no more overlap, using old correction a " << a << " b " << b;

//        if (b_new > 0.85)
//        {
//            a=a_new;
//            b=b_new;
//        }

        loginf << "MeasurementFlight: correctHeights: a " << a << " b " << b << " geo height corrected "
                <<  (current->getAltitude()-a)/b << " rrt " << baro_altitude_calculated << " tod " << timeStringFromDouble(current->getTod())
                << " with " << baro_data.size() << " samples";

        current->setAltitude((current->getAltitude()-a)/b);
    }

    loginf << "MeasurementFlight: correctHeights: done";
}

double MeasurementFlight::getHeightAtTime (const std::map<double, double> &data, double time, bool &success)
{
    //loginf << "MeasurementFlight: getHeightAtTime: data size " << data.size() << " time " << time;

    if (data.size() == 0)
    {
        success = false;
        return 0.0;
    }

    std::map <double, double>::const_iterator next_it = data.lower_bound(time);
    std::map <double, double>::const_iterator previous_it = next_it;

    if (previous_it == data.begin())
    {
        success = false;
        return 0.0;
    }
    previous_it--;

    if (previous_it == data.end() || next_it == data.end())
    {
        success = false;
        return 0.0;
    }
    assert (previous_it != next_it);

    double previous_alt = previous_it->second;
    double next_alt = next_it->second;

    double alt_diff = next_alt-previous_alt;
    double time_diff = next_it->first - previous_it->first;

    if (time_diff > 30) // huge gap - make no guess here
    {
        success = false;
        return 0.0;
    }

    assert (time_diff >= 0);

    double alt_diff_per_second = alt_diff/time_diff;

    double current_time_diff = time - previous_it->first;

    if (current_time_diff == 0)
    {
        success=true;
        return previous_alt;
    }

    assert (current_time_diff >= 0);

    double current_alt = previous_alt + current_time_diff * alt_diff_per_second;
    success = true;

    return current_alt;
}

void MeasurementFlight::getCorrectionParametersMLSQ (const std::vector <double> &baro, const std::vector<double> &geo, double &a, double &b)
{
    assert (baro.size() == geo.size());
    unsigned int n = baro.size();
    assert (n != 0);

    // x=baro, y=geo
    // y=a+b*x
    // y_adj = (y-a)/b;

//    double sum_x_times_y=0;
//    double sum_x=0;
//    double sum_y=0;
//    double sum_x_sqared=0;
//    double sum_x_outer_squared=0;
//    double n_fl = n;
//
//    double x, y;
//    for (unsigned int cnt=0; cnt < n; cnt++)
//    {
//        x = baro.at(cnt);
//        y = geo.at(cnt);
//
//        sum_x_times_y += x * y;
//        sum_x += x;
//        sum_y += y;
//        sum_x_sqared += x*x;
//    }
//    sum_x_outer_squared = sum_x*sum_x;
//
//    double b_nom = n_fl * sum_x_times_y - sum_x * sum_y;
//    double b_denom = n_fl * sum_x_sqared - sum_x_outer_squared;
//
//    assert (b_denom != 0);
//    b = b_nom / b_denom; // coooookiiiieee
//
//    double a_nom = sum_x_sqared * sum_y - sum_x * sum_x_times_y;
//    double a_denom = n_fl * sum_x_sqared - sum_x_outer_squared;
//
//    assert (a_denom != 0);
//    a = a_nom / a_denom; // nom nom nom

    double n_fl = n;
    double x_mean=0;
    double y_mean=0;

    for (unsigned int cnt=0; cnt < n; cnt++)
    {
        x_mean += baro.at(cnt);
        y_mean += geo.at(cnt);
    }

    x_mean /= n_fl;
    y_mean /= n_fl;

    double b_nom=0;
    double b_denom=0;

    double x, y;
    for (unsigned int cnt=0; cnt < n; cnt++)
    {
        x = baro.at(cnt);
        y = geo.at(cnt);

        b_nom += (x-x_mean)*(y-y_mean);
        b_denom += (x-x_mean)*(x-x_mean);
    }

    if (b_denom == 0)
        b=1;
    else
        b = b_nom/b_denom;

    if (b < 0.85)
        b=1;

    a = y_mean - b * x_mean;
}

}
