/*
 * ConvertDialog.h
 *
 *  Created on: Jul 3, 2014
 *      Author: sk
 */

#ifndef CONVERTDIALOG_H_
#define CONVERTDIALOG_H_

#include <QDialog>

class QLineEdit;
class QCheckBox;


namespace MFImport
{
class ConvertDialog : public QDialog
{
    Q_OBJECT
public slots:
    void ok ();
    void cancel ();

public:
    ConvertDialog(std::string name, QWidget *parent = 0);
    virtual ~ConvertDialog();

    std::string getName ();

    bool hasModeA ();
    unsigned int getModeA ();
    bool hasTargetAddress ();
    unsigned int getTargetAddress ();
    bool hasTargetIdentification ();
    std::string getTargetIdentification ();

protected:
    std::string name_; //intermediate name

    QLineEdit *name_edit_;
    QCheckBox *mode_a_check_;
    QLineEdit *mode_a_code_edit_;
    QCheckBox *target_address_check_;
    QLineEdit *target_address_edit_;
    QCheckBox *target_identification_check_;
    QLineEdit *target_identification_edit_;

    bool accepted_;

    void createGUIElements ();
};

} /* namespace MFImport */

#endif /* CONVERTDIALOG_H_ */
