/*
 * MeasurementFlightManager.h
 *
 *  Created on: Jul 3, 2014
 *      Author: sk
 */

#ifndef MEASUREMENTFLIGHTMANAGER_H_
#define MEASUREMENTFLIGHTMANAGER_H_

#include "Singleton.h"
#include "Configurable.h"
#include "BufferReceiver.h"

namespace MFImport
{
class MeasurementFlight;

class MeasurementFlightManager : public Singleton, public Configurable, public BufferReceiver
{
public:
    static MeasurementFlightManager& getInstance()
    {
      static MeasurementFlightManager instance;
      return instance;
    }
    virtual ~MeasurementFlightManager();

    virtual void generateSubConfigurable (std::string class_id, std::string instance_id);

    const std::map <std::string, MeasurementFlight*> &getFlights () { return flights_; }
    bool hasFlight(std::string name) { return flights_.find(name) != flights_.end(); }
    MeasurementFlight *getFlight (std::string name) { assert (hasFlight(name)); return flights_.at(name);}
    void deleteFlight (std::string name);

    void updateFlights ();

    void correctHeight();

    virtual void receive (Buffer *buffer);
    virtual void loadingDone ();

protected:
    std::map <std::string, MeasurementFlight*> flights_;
    std::map <double, double> track_heights_;

    MeasurementFlightManager();

    virtual void checkSubConfigurables () {}
};

} /* namespace MFImport */

#endif /* MEASUREMENTFLIGHTMANAGER_H_ */
