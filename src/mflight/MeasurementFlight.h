/*
 * MeasurementFlight.h
 *
 *  Created on: Jun 24, 2014
 *      Author: sk
 */

#ifndef MEASUREMENTFLIGHT_H_
#define MEASUREMENTFLIGHT_H_

#include "Configurable.h"
#include <map>

namespace MFImport
{
class GPSTrail;
class MeasurementFlightUpdate;

class MeasurementFlight : public Configurable
{
public:
    MeasurementFlight(std::string class_id, std::string instance_id, Configurable *parent);
    virtual ~MeasurementFlight();

    virtual void generateSubConfigurable (std::string class_id, std::string instance_id);

    void importGPSTrail (GPSTrail *trail, bool has_mode_a, unsigned int mode_a, bool has_ta, unsigned int ta,
            bool has_ti, std::string ti); // only for new flight

    const std::map <double, MeasurementFlightUpdate*> &getUpdates () { return updates_; }
    bool hasUpdates () { return updates_.size() != 0; }
    unsigned int getNumUpdates () { return updates_.size(); }

    const std::string &getName () { return name_; }
    void setName (std::string value) { name_=value; }

    double getTODMinimum () { assert (hasUpdates()); return tod_min_; }
    double getTODMaximum () { assert (hasUpdates()); return tod_max_; }
    double getTODDuration () { assert (hasUpdates()); assert (tod_min_<=tod_max_); return tod_max_-tod_min_; }

    const std::set<unsigned int> &getModeACodes () { return mode_a_codes_; }
    const std::set<unsigned int> &getTargetAddresses () { return target_addresses_; }
    const std::set<std::string> &getTargetIdentifications () { return target_identificatons_; }

    bool hasModeACodes () { return mode_a_codes_.size() != 0; }
    bool hasTargetAddresses () { return target_addresses_.size() != 0; }
    bool hasTargetIdentifications () { return target_identificatons_.size() != 0; }

    std::string getModeACodesAsString ();
    std::string getTargetAddressesAsString ();
    std::string getTargetIdentificationsAsString ();

    void correctHeights (const std::map <double, double> &height_data);

protected:
    std::string name_;

    double tod_min_;
    double tod_max_;

    std::set<unsigned int> mode_a_codes_;
    std::set<unsigned int> target_addresses_;
    std::set<std::string> target_identificatons_;

    std::map <double, MeasurementFlightUpdate*> updates_;

    virtual void checkSubConfigurables () {}
    double getHeightAtTime (const std::map<double, double> &data, double time, bool &success);
    void getCorrectionParametersMLSQ (const std::vector <double> &baro, const std::vector<double> &geo, double &a, double &b);
};
}
#endif /* MEASUREMENTFLIGHT_H_ */
