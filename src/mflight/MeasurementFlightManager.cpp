/*
 * MeasurementFlightManager.cpp
 *
 *  Created on: Jul 3, 2014
 *      Author: sk
 */

#include "MeasurementFlightManager.h"
#include "MeasurementFlight.h"
#include "ATSDB.h"
#include "DBOVariableSet.h"
#include "InsertManager.h"
#include "String.h"
#include "DBObjectManager.h"
#include "DBOVariable.h"

using namespace Utils::String;

namespace MFImport
{

MeasurementFlightManager::MeasurementFlightManager()
: Singleton(), Configurable ("MeasurementFlightManager", "MeasurementFlightManager0", 0, "conf/config_mflights.xml")
{
    createSubConfigurables();

}

MeasurementFlightManager::~MeasurementFlightManager()
{
    std::map <std::string, MeasurementFlight*>::iterator it;

    for (it = flights_.begin(); it != flights_.end(); it++)
        delete it->second;
    flights_.clear();
}

void MeasurementFlightManager::generateSubConfigurable (std::string class_id, std::string instance_id)
{
    if (class_id == "MeasurementFlight")
    {
        MeasurementFlight *flight = new MeasurementFlight (class_id, instance_id, this);
        if (flights_.find(flight->getName()) != flights_.end())
        {
            logwrn << "MeasurementFlightManager: generateSubConfigurable: flight with name '" << flight->getName() << "' already exists, removing";
            delete flight;
            return;
        }

        flights_[flight->getName()] = flight;
    }
    else
        throw std::runtime_error ("MeasurementFlightManager: generateSubConfigurable: unknown class_id "+class_id);
}

void MeasurementFlightManager::updateFlights ()
{
    std::map <std::string, MeasurementFlight*> old = flights_;
    std::map <std::string, MeasurementFlight*>::iterator it;

    flights_.clear();

    for (it = old.begin(); it != old.end(); it++)
    {
        assert (flights_.find(it->second->getName()) == flights_.end());
        flights_[it->second->getName()] = it->second;
    }
}

void MeasurementFlightManager::deleteFlight (std::string name)
{
    assert (hasFlight(name));
    std::map <std::string, MeasurementFlight*>::iterator it;
    it = flights_.find(name);
    MeasurementFlight *flight = it->second;

    flights_.erase(it);
    delete flight;
    return;
}

void MeasurementFlightManager::correctHeight()
{
    loginf << "MeasurementFlightManager: correctHeight";

    track_heights_.clear();

    DBOVariableSet read_list;

    assert (DBObjectManager::getInstance().existsDBOVariable(DBO_SYSTEM_TRACKS, "TOD"));
    DBOVariable *tod = DBObjectManager::getInstance().getDBOVariable(DBO_SYSTEM_TRACKS, "TOD");
    read_list.add(tod);

    assert (DBObjectManager::getInstance().existsDBOVariable(DBO_SYSTEM_TRACKS, "MODEC_CODE_FT"));
    DBOVariable *height = DBObjectManager::getInstance().getDBOVariable(DBO_SYSTEM_TRACKS, "MODEC_CODE_FT");
    read_list.add(height);

    std::string custom_filter_clause;
    custom_filter_clause = "track_num="+intToString (InsertManager::getInstance().getCurrentTrackNum());

    ATSDB::getInstance().startReading (this, DBO_SYSTEM_TRACKS, read_list, custom_filter_clause, tod);
}

void MeasurementFlightManager::receive (Buffer *buffer)
{
    assert (buffer);

    if (buffer->getFirstWrite())
        throw std::runtime_error ("No Track Data Found");

    unsigned int size=buffer->getSize();
    std::vector <void*> *addresses;

    buffer->setIndex(0);
    double tod;
    double height;

    assert (buffer->getAdresses()->size() == 2);

    for (unsigned int cnt = 0; cnt < size; cnt++)
    {
        if (cnt != 0)
            buffer->incrementIndex();

        addresses = buffer->getAdresses();

        tod = *(double*) addresses->at(0);
        height = *(int*) addresses->at(1);
        //tod /= 128.0;

        //loginf << "cnt " << cnt << " tod " << tod << " height " << height;

        track_heights_[tod] = height;
    }

    loginf << "MeasurementFlightManager: receive: size " << buffer->getSize();
    delete buffer;
}
void MeasurementFlightManager::loadingDone ()
{
    assert (track_heights_.size() != 0);

    loginf << "MeasurementFlightManager: loadingDone: num heights " << track_heights_.size();

    if (!hasFlight(InsertManager::getInstance().getCurrentMeasurementFlight()))
        throw std::runtime_error ("Current Measurement Flight does not exist");

    getFlight(InsertManager::getInstance().getCurrentMeasurementFlight())->correctHeights(track_heights_);
}
} /* namespace MFImport */
