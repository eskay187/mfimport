/*
 * MeasurementFlightUpdate.h
 *
 *  Created on: Jun 24, 2014
 *      Author: sk
 */

#ifndef MEASUREMENTFLIGHTUPDATE_H_
#define MEASUREMENTFLIGHTUPDATE_H_

#include "GPSUpdate.h"

namespace MFImport
{
class MeasurementFlightUpdate : public GPSUpdate
{
public:
    MeasurementFlightUpdate (std::string class_id, std::string instance_id, Configurable *parent)
    : GPSUpdate (class_id, instance_id, parent)
    {
        registerParameter ("has_mode_a_code", &has_mode_a_code_, false);
        registerParameter ("mode_a_code", &mode_a_code_, 4096);

        if (has_mode_a_code_)
            assert (mode_a_code_ < 4096);

        registerParameter ("has_target_address", &has_target_address_, false);
        registerParameter ("target_address", &target_address_, 0xFFFFFF+1);

        if (has_target_address_)
            assert (target_address_ != 0xFFFFFF+1);

        registerParameter ("has_target_identification", &has_target_identification_, false);
        registerParameter ("target_identification", &target_identification_, (std::string) "");

        if (has_target_identification_)
            assert (target_identification_.size() != 0);
    }
    ~MeasurementFlightUpdate() {}

    bool hasModeACode() const
    {
        return has_mode_a_code_;
    }

    unsigned int getModeACode() const
    {
        assert (has_mode_a_code_);
        return mode_a_code_;
    }

    void setModeACode(unsigned int modeACode)
    {
        has_mode_a_code_=true;
        mode_a_code_ = modeACode;
    }

    bool hasTargetAddress() const
    {
        return has_target_address_;
    }

    unsigned int getTargetAddress() const
    {
        assert (has_target_address_);
        return target_address_;
    }

    void setTargetAddress(unsigned int targetAddress)
    {
        has_target_address_=true;
        target_address_ = targetAddress;
    }

    bool hasTargetIdentification() const
    {
        return has_target_identification_;
    }

    const std::string& getTargetIdentification() const
    {
        assert (has_target_identification_);
        return target_identification_;
    }

    void setTargetIdentification(const std::string& targetIdentification)
    {
        has_target_identification_=true;
        target_identification_ = targetIdentification;
    }

protected:
    bool has_mode_a_code_;
    unsigned int mode_a_code_;
    bool has_target_address_;
    unsigned int target_address_;
    bool has_target_identification_;
    std::string target_identification_;
};
}


#endif /* MEASUREMENTFLIGHTUPDATE_H_ */
