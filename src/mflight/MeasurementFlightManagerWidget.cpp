/*
 * MeasurementFlightManagerWidget.cpp
 *
 *  Created on: Jul 3, 2014
 *      Author: sk
 */

#include "MeasurementFlightManagerWidget.h"
#include "MeasurementFlightManager.h"
#include "MeasurementFlight.h"
#include "GPSTrail.h"
#include "String.h"
#include "ConvertDialog.h"
#include "InsertManager.h"

#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QPushButton>
#include <QTableWidget>
#include <QTableWidgetItem>
#include <QLineEdit>
#include <QMessageBox>

using namespace Utils::String;

namespace MFImport
{

MeasurementFlightManagerWidget::MeasurementFlightManagerWidget(QWidget* parent, Qt::WindowFlags f)
: QWidget (parent, f), table_(0)
{
    createGUIElements ();
    updateFlightTable ();

}

MeasurementFlightManagerWidget::~MeasurementFlightManagerWidget()
{
}

void MeasurementFlightManagerWidget::createGUIElements ()
{
    QFont font_bold;
    font_bold.setBold(true);

    QFont font_big;
    font_big.setPointSize(18);

    QVBoxLayout *hlayout = new QVBoxLayout();

    QLabel *label = new QLabel ("Measurement Flights");
    label->setFont (font_big);
    hlayout->addWidget (label);

    // MF table
    table_ = new QTableWidget ();
    table_->setSelectionBehavior(QAbstractItemView::SelectRows);
    table_->setSelectionMode(QAbstractItemView::SingleSelection);

    QStringList header_list;
    header_list.append (tr("Name"));
    header_list.append (tr("Time"));
    header_list.append (tr("Duration"));
    header_list.append (tr("Mode A"));
    header_list.append (tr("Target Address"));
    header_list.append (tr("Target Identification"));
    header_list.append (tr("Delete"));
    header_list.append (tr("Insert"));
    table_->setColumnCount(header_list.size());
    table_->setHorizontalHeaderLabels (header_list);

    hlayout->addWidget (table_);

    //buttons
//    QHBoxLayout *blayout = new QHBoxLayout ();
//
//    blayout->addStretch();
//
//    QPushButton *import = new QPushButton ("Import");
//    import->setToolTip("Import new GPS Trail");
//    connect (import, SIGNAL(clicked ()), this, SLOT (import()));
//    blayout->addWidget (import);
//
//    hlayout->addLayout (blayout);

    setLayout(hlayout);
}

void MeasurementFlightManagerWidget::updateFlightTable ()
{
    assert (table_);
    table_->clearContents();

    name_items_.clear();
    delete_buttons_.clear();
    insert_buttons_.clear();

    QPixmap* pixmapdel = new QPixmap("icons/close_icon.png");
    QPixmap* pixmapins = new QPixmap("icons/insert.png");

    const std::map <std::string, MeasurementFlight*> &trails = MeasurementFlightManager::getInstance().getFlights ();
    std::map <std::string, MeasurementFlight*>::const_iterator it;

    table_->setRowCount(trails.size());

    unsigned int row = 0;
    QTableWidgetItem *newItem=0;
    MeasurementFlight *flight;
    for (it = trails.begin(); it != trails.end(); it++)
    {
        flight = it->second;

        QLineEdit *name_edit = new QLineEdit(flight->getName().c_str());
        name_items_[name_edit] = flight->getName();
        name_edit->setMinimumWidth(300);
        table_->setCellWidget(row, 0, name_edit);
        connect (name_edit, SIGNAL(returnPressed()), this, SLOT(nameChanged()));

        std::string tmp = timeStringFromDouble(flight->getTODMinimum()) + " - " + timeStringFromDouble(flight->getTODMaximum());
        newItem = new QTableWidgetItem(tmp.c_str());
        newItem->setFlags(Qt::ItemIsEnabled);
        table_->setItem(row, 1, newItem);

        newItem = new QTableWidgetItem(timeStringFromDouble(flight->getTODDuration()).c_str());
        newItem->setFlags(Qt::ItemIsEnabled);
        table_->setItem(row, 2, newItem);

        newItem = new QTableWidgetItem();
        if (flight->hasModeACodes())
            newItem->setText(flight->getModeACodesAsString().c_str());
        else
            newItem->setBackgroundColor(Qt::lightGray);
        newItem->setFlags(Qt::ItemIsEnabled);
        table_->setItem(row, 3, newItem);

        newItem = new QTableWidgetItem();
        if (flight->hasTargetAddresses())
            newItem->setText(flight->getTargetAddressesAsString().c_str());
        else
            newItem->setBackgroundColor(Qt::lightGray);
        newItem->setFlags(Qt::ItemIsEnabled);
        table_->setItem(row, 4, newItem);

        newItem = new QTableWidgetItem();
        if (flight->hasTargetIdentifications())
            newItem->setText(flight->getTargetIdentificationsAsString().c_str());
        else
            newItem->setBackgroundColor(Qt::lightGray);
        newItem->setFlags(Qt::ItemIsEnabled);
        table_->setItem(row, 5, newItem);

        QPushButton *del = new QPushButton ();
        del->setIcon(QIcon(*pixmapdel));
        del->setFlat(true);
        del->setToolTip(tr("Delete trail"));
        table_->setCellWidget(row, 6, del);
        delete_buttons_[del] = it->second->getName();
        connect (del, SIGNAL(clicked()), this, SLOT(deleteFlight()));

        QPushButton *ins = new QPushButton ();
        ins->setIcon(QIcon(*pixmapins));
        ins->setFlat(true);
        ins->setToolTip(tr("Insert trail"));
        table_->setCellWidget(row, 7, ins);
        insert_buttons_[ins] = it->second->getName();
        connect (ins, SIGNAL(clicked()), this, SLOT(insertFlight()));

        row++;
    }

    table_->resizeColumnsToContents();
}

void MeasurementFlightManagerWidget::nameChanged ()
{
    QLineEdit *edit = dynamic_cast<QLineEdit*> (QObject::sender());
    assert (edit);
    assert (name_items_.find (edit) != name_items_.end());

    std::string name = name_items_[edit];
    std::string new_name = edit->text().toStdString();
    assert (MeasurementFlightManager::getInstance().hasFlight(name));

    if (MeasurementFlightManager::getInstance().hasFlight(new_name))
    {
        QMessageBox::warning ( this, "Measurement Flight Name Change", "Measurement Flight with name "+edit->text()+" already exists");
        edit->setText (name.c_str());
        return;
    }

    MeasurementFlight *flight = MeasurementFlightManager::getInstance().getFlight(name);

    flight->setName(new_name);

    MeasurementFlightManager::getInstance().updateFlights();

    loginf << "MeasurementFlightManagerWidget: nameChanged: flight name changed to " << new_name;

    updateFlightTable();
}

void MeasurementFlightManagerWidget::deleteFlight ()
{
    QPushButton *button = dynamic_cast<QPushButton*> (QObject::sender());
    assert (button);
    assert (delete_buttons_.find(button) != delete_buttons_.end());

    std::string name = delete_buttons_[button];
    assert (MeasurementFlightManager::getInstance().hasFlight(name));
    MeasurementFlightManager::getInstance().deleteFlight(name);

    loginf << "MeasurementFlightManagerWidget: deleteFlight: deleted flight " << name;

    if (InsertManager::getInstance().getCurrentMeasurementFlight() == name)
        emit insertFlight ("");

    updateFlightTable();
}

void MeasurementFlightManagerWidget::insertFlight ()
{
    QPushButton *button = dynamic_cast<QPushButton*> (QObject::sender());
    assert (button);
    assert (insert_buttons_.find(button) != insert_buttons_.end());

    std::string name = insert_buttons_[button];
    assert (MeasurementFlightManager::getInstance().hasFlight(name));

    emit insertFlight (name);
//    MeasurementFlightManager::getInstance().deleteFlight(name);
//
//    loginf << "MeasurementFlightManagerWidget: deleteFlight: deleted flight " << name;
//
//    updateFlightTable();
}

void MeasurementFlightManagerWidget::convert (GPSTrail *trail)
{
    assert (trail);

    loginf << "MeasurementFlightManagerWidget: convert: trail " << trail->getName();

    ConvertDialog dialog (trail->getName(), this);

    int ret = dialog.exec();

    //loginf << "UGA ret " << ret << " cancel " << QMessageBox::Cancel << " ok " << QMessageBox::Ok;

    if (ret == QDialog::Rejected)
        return;

    if (ret == QDialog::Accepted)
    {
        loginf << "MeasurementFlightManagerWidget: convert: ok received, importing";

        std::string name = dialog.getName();
        bool has_ma = dialog.hasModeA();
        unsigned int ma = has_ma ? dialog.getModeA() : 0;
        bool has_ta = dialog.hasTargetAddress();
        unsigned int ta = has_ta ? dialog.getTargetAddress() : 0;
        bool has_ti = dialog.hasTargetIdentification();
        std::string ti = has_ti ? dialog.getTargetIdentification() : "";

        assert (!MeasurementFlightManager::getInstance().hasFlight(name));

        loginf << "MeasurementFlightManagerWidget: convert: creating measurement flight";

        Configuration &config = MeasurementFlightManager::getInstance().addNewSubConfiguration("MeasurementFlight");
        config.addParameterString("name", name);
        MeasurementFlightManager::getInstance().generateSubConfigurable(config.getClassId(), config.getInstanceId());

        assert (MeasurementFlightManager::getInstance().hasFlight(name));

        MeasurementFlight *flight = MeasurementFlightManager::getInstance().getFlight(name);
        assert (flight);

        loginf << "MeasurementFlightManagerWidget: convert: measurement flight updates import";
        flight->importGPSTrail(trail, has_ma, ma, has_ta, ta, has_ti, ti);
        loginf << "MeasurementFlightManagerWidget: convert: measurement flight updates import done";
        updateFlightTable();
    }

    logdbg << "MeasurementFlightManagerWidget: convert: done";
}

} /* namespace MFImport */
