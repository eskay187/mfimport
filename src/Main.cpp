
#include <exception>

#include "Config.h"
#include "MFImport.h"
#include "MainWindow.h"
#include "Logger.h"
#include "WorkerThreadManager.h"
#include "ProjectionManager.h"
#include "String.h"

using namespace Utils::String;

int main(int argc, char **argv)
{
    std::string db_type;
    std::string db_filename;
    std::string db_server;
    std::string db_name;
    std::string db_port;
    std::string db_user;
    std::string db_password;
    bool db_no_password=false;
    std::string db_schema;
    bool auto_start=false;

    Config::getInstance().init ("conf/mfimport.conf");
    Logger::getInstance();

    loginf  << "Copyright (C) by Graz University of Technology 2014. All rights reserved.";

    if (argc > 1)
    {
        for (int cnt=0; cnt < argc; cnt++)
        {
            //loginf  << "MFImport: Main: got argument " << cnt << ": '" << argv[cnt] <<"'";
            std::string argument (argv[cnt]);
            if (std::string::npos != argument.find("mfimport"))
            {
                // our start command
            }
            else if (argument.compare("--help") == 0)
            {
                loginf << "MFImport Main: help: Command line arguments:" << log4cpp::eol
                        << "'--help'                        Print this help" << log4cpp::eol
                        << "'--config-reset <value>'        Reset configuration to saved one; value examples 'save_v6','save_v7', 'save_rdl'" << log4cpp::eol
                        << "'--system-center <values>'      Set system center projection point, as comma separated latitude (°East), longitude (°North), altitude (meters above MSL); value example '47.5,14,0'" << log4cpp::eol
                        << "'--database-type <value>'       Set database type; value examples 'mysqlpp', 'mysqlcon', 'sqlite'" << log4cpp::eol
                        << "'--database-filename <value>'   Set sqlite database filename; value example 'data/test.rdb'" << log4cpp::eol
                        << "'--database-server <value>'     Set mysql database server name; value examples 'localhost', '192.168.0.2'" << log4cpp::eol
                        << "'--database-name <value>'       Set mysql database name; value example 'job_v7mainroot_0003'" << log4cpp::eol
                        << "'--database-port <value>'       Set mysql database server port; value example '3306'" << log4cpp::eol
                        << "'--database-user <value>'       Set mysql database user; value example 'root'" << log4cpp::eol
                        << "'--database-no-password'        Set mysql database user password to empty" << log4cpp::eol
                        << "'--database-password <value>'   Set mysql database user password" << log4cpp::eol
                        << "'--database-schema <value>'     Set database schema; value examples 'v6', 'v7', 'rdl'" << log4cpp::eol
                        << "'--auto-start'                  Start mfimport without database configuration dialog" << log4cpp::eol;
                loginf << "MFImport help: mysql example command line: " << log4cpp::eol
                        << "'./mfimport --system-center 47.5,14,0 --database-type mysql --database-server localhost --database-name job_v7mainroot_0003 --database-port 3306 --database-user root --database-no-password --auto-start'" << log4cpp::eol;
                loginf << "MFImport help: sqlite example command line: " << log4cpp::eol
                        << "'./mfimport --system-center 47.5,14,0 --database-type sqlite --database-filename test.rdb'" << log4cpp::eol;
                return 0;
            }
            else if (argument.compare("--config-reset") == 0)
            {
                if (cnt+1 >= argc)
                {
                    logerr  << "MFImport: Main: command '--config-reset' given, but no value supplied";
                    return 0;
                }
                std::string config = argv[cnt+1];

//                if (config.compare("save_v6") == 0 || config.compare("save_v7") == 0 || config.compare("save_rdl") == 0)
//                {
                    config = "cp conf/"+config+"/*.xml conf/ -v";
                    system(config.c_str());
//                }
//                else
//                {
//                    logerr  << "MFImport: Main: command '--config-reset' given, but incorrect value '" << config << "' supplied";
//                    return 0;
//                }

                loginf  << "MFImport: Main: database filename set to " << db_filename;
                cnt++;
            }
            else if (argument.compare("--database-type") == 0)
            {
                if (cnt+1 >= argc)
                {
                    logerr  << "MFImport: Main: command '--database-type' given, but no value supplied";
                    return 0;
                }
                db_type = argv[cnt+1];

                loginf  << "MFImport: Main: database type set to " << db_type;
                cnt++;
            }
            else if (argument.compare("--database-filename") == 0)
            {
                if (cnt+1 >= argc)
                {
                    logerr  << "MFImport: Main: command '--database-filename' given, but no value supplied";
                    return 0;
                }
                db_filename = argv[cnt+1];

                loginf  << "MFImport: Main: database filename set to " << db_filename;
                cnt++;
            }
            else if (argument.compare("--system-center") == 0)
            {
                if (cnt+1 >= argc)
                {
                    logerr  << "MFImport: Main: command '--system-center' given, but no values supplied";
                    return 0;
                }
                std::string center (argv[cnt+1]);
                std::vector<std::string> values;
                values = split(center, ',', values);
                if (values.size() != 3)
                {
                    logerr  << "MFImport: Main: command '--system-center' given, but only " << values.size() << " values supplied in '" << center << "'";
                    return 0;
                }

                double lat = doubleFromString(values.at(0));
                double lon = doubleFromString(values.at(1));
                double alt = doubleFromString(values.at(2));
                ProjectionManager::getInstance().setCenterLatitude(lat);
                ProjectionManager::getInstance().setCenterLongitude(lon);
                ProjectionManager::getInstance().setCenterAltitude(alt);
                loginf  << "MFImport: Main: system center set to altitude " << lat << " longitude " << lon << " altitude " << alt;
                cnt++;
            }
            else if (argument.compare("--database-server") == 0)
            {
                if (cnt+1 >= argc)
                {
                    logerr  << "MFImport: Main: command '--database-server' given, but no value supplied";
                    return 0;
                }
                std::string value (argv[cnt+1]);

                loginf  << "MFImport: Main: database server set to " << value;
                cnt++;
            }
            else if (argument.compare("--database-name") == 0)
            {
                if (cnt+1 >= argc)
                {
                    logerr  << "MFImport: Main: command '--database-name' given, but no value supplied";
                    return 0;
                }
                db_name = argv[cnt+1];
                loginf  << "MFImport: Main: database name set to " << db_name;
                cnt++;
            }
            else if (argument.compare("--database-port") == 0)
            {
                if (cnt+1 >= argc)
                {
                    logerr  << "MFImport: Main: command '--database-port' given, but no value supplied";
                    return 0;
                }
                db_port = argv[cnt+1];
                loginf  << "MFImport: Main: database server port to " << db_port;
                cnt++;
            }
            else if (argument.compare("--database-user") == 0)
            {
                if (cnt+1 >= argc)
                {
                    logerr  << "MFImport: Main: command '--database-user' given, but no value supplied";
                    return 0;
                }
                db_user = argv[cnt+1];
                loginf  << "MFImport: Main: database user to " << db_user;
                cnt++;
            }
            else if (argument.compare("--database-password") == 0)
            {
                if (cnt+1 >= argc)
                {
                    logerr  << "MFImport: Main: command '--database-password' given, but no value supplied";
                    return 0;
                }
                db_password = argv[cnt+1];
                loginf  << "MFImport: Main: database password to " << db_password;
                cnt++;
            }
            else if (argument.compare("--database-no-password") == 0)
            {
                db_no_password=true;
                loginf  << "MFImport: Main: database no password set";
            }
            else if (argument.compare("--database-schema") == 0)
            {
                if (cnt+1 >= argc)
                {
                    logerr  << "MFImport: Main: command '--database-schema' given, but no value supplied";
                    return 0;
                }
                db_schema = argv[cnt+1];

                loginf  << "MFImport: Main: database schema set to " << db_schema;
                cnt++;
            }
            else if (argument.compare("--auto-start") == 0)
            {
                auto_start=true;
                loginf  << "MFImport: Main: auto start set";
            }
            else
            {
                logerr  << "MFImport: Main: unknown command '" << argv[cnt] << "'";
                return 0;
            }
        }
    }

    try
    {
        MFImport::MFImport mf(argc, argv);

        MainWindow window;

        if (db_type.size() > 0)
            window.setDBType(db_type);

        if (db_filename.size() > 0)
            window.setDBFilename(db_filename);

        if (db_server.size() > 0)
            window.setDBServer(db_server);

        if (db_name.size() > 0)
            window.setDBName(db_name);

        if (db_port.size() > 0)
            window.setDBPort(db_port);

        if (db_user.size() > 0)
            window.setDBUser(db_user);

        if (db_password.size() > 0)
            window.setDBPassword(db_password);

        if (db_no_password)
            window.setDBNoPassword();

        if (db_schema.size() > 0)
            window.setDBSchema(db_schema);

        if (auto_start)
            window.triggerAutoStart();

        window.show();

        return mf.exec();
    }
    catch (std::exception &ex)
    {
        logerr  << "Main: Caught Exception '" << ex.what() << "'";

        WorkerThreadManager::getInstance().shutdown();

        return -1;
    }
    catch(...)
    {
        logerr  << "Main: exec: Caught: Exception";

        WorkerThreadManager::getInstance().shutdown();

        return -1;
    }
    return 0;
}

