/*
 * FormatDefinitionColumn.h
 *
 *  Created on: Jun 25, 2014
 *      Author: sk
 */

#ifndef FORMATDEFINITIONCOLUMN_H_
#define FORMATDEFINITIONCOLUMN_H_

#include "Configurable.h"

namespace MFImport
{
class GPSTrail;


enum FORMAT_DEFINITION_COLUMN_TYPE { COLUMN_TOD=0, COLUMN_LATITUDE, COLUMN_LONGITUDE, COLUMN_ALTITUDE,  COLUMN_SKIP, COLUMN_STOP, COLUMN_SENTINEL };

extern std::map<FORMAT_DEFINITION_COLUMN_TYPE,std::string> FORMAT_DEFINITION_COLUMN_TYPE_STRINGS;

class FormatDefinitionColumn : public Configurable
{
public:
    FormatDefinitionColumn(std::string class_id, std::string instance_id, Configurable *parent);
    virtual ~FormatDefinitionColumn();

    virtual void generateSubConfigurable (std::string class_id, std::string instance_id);

    void parse (std::string item_string, GPSTrail *trail, bool &stop); // parses item, adds to trail , sets stop flag

    const std::string& getName() const { return name_; }

    void setName(const std::string& name) { name_ = name; }

    int getOrder() const { return order_; }

    void setOrder(int order) { order_ = order; }

    FORMAT_DEFINITION_COLUMN_TYPE getType() const { return (FORMAT_DEFINITION_COLUMN_TYPE) type_int_; }

    void setType(FORMAT_DEFINITION_COLUMN_TYPE type) { type_int_ = type; }

protected:
    int order_;
    std::string name_;
    int type_int_;
};

} /* namespace MFImport */

#endif /* FORMATDEFINITIONCOLUMN_H_ */
