/*
 * FormatDefinition.h
 *
 *  Created on: Jun 24, 2014
 *      Author: sk
 */

#ifndef FORMATDEFINITION_H_
#define FORMATDEFINITION_H_

#include "Configurable.h"

namespace MFImport
{

class FormatDefinitionColumn;
class GPSTrail;

class FormatDefinition : public Configurable
{
public:
    FormatDefinition(std::string class_id, std::string instance_id, Configurable *parent);
    virtual ~FormatDefinition();

    void setName (std::string value);
    const std::string getName () { return name_; }

    const std::string getSeparationCharacter () { return separation_character_; }
    void setSeparationCharacter (const char value) { separation_character_=value; }

    const std::string getCommentCharacter () { return comment_character_; }
    void setCommentCharacter (const char value) { comment_character_=value; }

    const std::map <unsigned int, FormatDefinitionColumn*> &getColumns () { return columns_; }
    bool hasColumn (std::string name);
    FormatDefinitionColumn *getColumn (std::string name);
    void deleteColumn (std::string name);
    void moveColumnUp (std::string name);
    void moveColumnDown (std::string name);

    bool parse (unsigned int line_cnt, const std::string &line, GPSTrail *trail, std::stringstream &ss, bool log_flag);
    // parses line, adds a GPSUpdate into trail. trail can be 0 in case of test flag 1

    virtual void generateSubConfigurable (std::string class_id, std::string instance_id);

    unsigned int getMaxOrder ();
    bool hasAllColumns ();

    bool getAltitudeM2ft() const { return altitude_m2ft_;  }
    void setAltitudeM2ft(bool altitudeM2ft){ altitude_m2ft_ = altitudeM2ft; }

    bool getGeoCoordinatesRad2deg() const { return geo_coordinates_rad2deg_; }
    void setGeoCoordinatesRad2deg(bool geoCoordinatesRad2deg) { geo_coordinates_rad2deg_ = geoCoordinatesRad2deg;}

    bool getHasTimeConstant() const { return has_time_constant_; }
    double getTimeConstant() const { return time_constant_; }
    void setTimeConstant(double timeConstant) { has_time_constant_=true; time_constant_ = timeConstant; }

protected:
    std::string name_;

    std::string separation_character_;
    std::string comment_character_;

    bool geo_coordinates_rad2deg_;
    bool altitude_m2ft_;
    bool has_time_constant_;
    double time_constant_;

    std::map <unsigned int, FormatDefinitionColumn*> columns_; //order ->column

    bool has_last_update_;
    double last_tod;
    double last_latitude;
    double last_longitude;
    double last_altitude;

    virtual void checkSubConfigurables () {}
};
}

#endif /* FORMATDEFINITION_H_ */
