/*
 * FormatDefinition.cpp
 *
 *  Created on: Jun 24, 2014
 *      Author: sk
 */

#include "FormatDefinition.h"
#include "FormatDefinitionColumn.h"
#include "FormatDefinitionManager.h"
#include "GPSTrail.h"

#include <algorithm>
#include <math.h>

namespace MFImport
{

FormatDefinition::FormatDefinition(std::string class_id, std::string instance_id, Configurable *parent)
: Configurable (class_id, instance_id, parent), has_last_update_(false), last_tod(0), last_latitude(0), last_longitude(0), last_altitude(0)
{
    registerParameter ("name", &name_, "");
    assert (name_.size() != 0);

    registerParameter ("separation_character", &separation_character_, ",");
    registerParameter ("comment_character", &comment_character_, "#");

    registerParameter ("geo_coordinates_rad2deg", &geo_coordinates_rad2deg_, false);
    registerParameter ("altitude_m2ft", &altitude_m2ft_, false);
    registerParameter ("has_time_constant", &has_time_constant_, false);
    registerParameter ("time_constant", &time_constant_, 0.0);

    assert (separation_character_.size() == 1);
    assert (comment_character_.size() == 1);

    createSubConfigurables();
}

FormatDefinition::~FormatDefinition()
{

    std::map <unsigned int, FormatDefinitionColumn*>::iterator it;

    for (it = columns_.begin(); it != columns_.end(); it++)
    {
        delete it->second;
    }
    columns_.clear();

}

void FormatDefinition::setName (std::string value)
{
    assert (value.size() > 0);
    name_=value;

    FormatDefinitionManager::getInstance().updateFormats();
}

void FormatDefinition::generateSubConfigurable (std::string class_id, std::string instance_id)
{
    if (class_id == "FormatDefinitionColumn")
    {
        FormatDefinitionColumn *col = new FormatDefinitionColumn (class_id, instance_id, this);

        if (columns_.find(col->getOrder()) != columns_.end())
        {
            logwrn << "FormatDefinition: generateSubConfigurable: column with order " << col->getOrder() << " already exists, deleting";
            delete col;
            return;
        }

        columns_[col->getOrder()] = col;
    }
}

bool FormatDefinition::hasColumn (std::string name)
{
    std::map <unsigned int, FormatDefinitionColumn*>::iterator it;

    for (it = columns_.begin(); it != columns_.end(); it++)
        if (it->second->getName() == name)
            return true;

    return false;
}

FormatDefinitionColumn *FormatDefinition::getColumn (std::string name)
{
    assert (hasColumn(name));

    std::map <unsigned int, FormatDefinitionColumn*>::iterator it;

    for (it = columns_.begin(); it != columns_.end(); it++)
        if (it->second->getName() == name)
            return it->second;

    throw std::runtime_error ("FormatDefinition: getColumn: column "+name+" not found");
}

void FormatDefinition::deleteColumn (std::string name)
{
    assert (hasColumn(name));

    std::map <unsigned int, FormatDefinitionColumn*>::iterator it;

    for (it = columns_.begin(); it != columns_.end(); it++)
        if (it->second->getName() == name)
        {
            FormatDefinitionColumn *col = it->second;
            delete col;
            columns_.erase(it);
            return;
        }

    throw std::runtime_error ("FormatDefinition: deleteColumn: column "+name+" not found");
}

void FormatDefinition::moveColumnDown (std::string name)
{
    assert (hasColumn(name));

    std::map <unsigned int, FormatDefinitionColumn*>::iterator it, itlast;

    for (it = columns_.begin(); it != columns_.end(); it++)
        if (it->second->getName() == name)
        {
            itlast = it;
            it++;

            if (it == columns_.end())
            {
                logerr  << "DBOVariableOrderedSet: moveColumnDown: tried to move down last variable";
                return;
            }

            std::swap (itlast->second, it->second);
            it->second->setOrder(it->first);
            itlast->second->setOrder(itlast->first);
            return;
        }

    throw std::runtime_error ("FormatDefinition: moveColumnDown: column "+name+" not found");
}

void FormatDefinition::moveColumnUp (std::string name)
{
    assert (hasColumn(name));

    std::map <unsigned int, FormatDefinitionColumn*>::iterator it, itlast;

    itlast = columns_.begin();
    for (it = columns_.begin(); it != columns_.end(); it++)
    {
        if (it->second->getName() == name)
        {
            if (it == itlast)
            {
                logerr  << "DBOVariableOrderedSet: moveVariableUp: tried to move up first variable";
                return;
            }

            std::swap (itlast->second, it->second);
            it->second->setOrder(it->first);
            itlast->second->setOrder(itlast->first);
            return;
        }
        itlast=it;
    }

    throw std::runtime_error ("FormatDefinition: moveColumnUp: column "+name+" not found");
}


unsigned int FormatDefinition::getMaxOrder ()
{
    if (columns_.size() == 0)
        return 0;

    return columns_.rbegin()->first+1;
}

bool FormatDefinition::parse (unsigned int line_cnt, const std::string &line, GPSTrail *trail, std::stringstream &ss, bool log_flag)
{
    if (line_cnt == 0)
        has_last_update_=false;

    std::string line_no_spaces = line;
    line_no_spaces.erase(std::remove(line_no_spaces.begin(), line_no_spaces.end(), ' '), line_no_spaces.end());

    if (line_no_spaces.size() == 0)
    {
        if (log_flag)
            ss << "Line " << line_cnt << ": Empty, no data added" << std::endl;
        return true;
    }

    if (line_no_spaces.at(0) == comment_character_.at(0))
    {
        if (log_flag)
            ss << "Line " << line_cnt << ": Comment, no data added" << std::endl;
        return true;
    }

    std::vector<std::string> parts = Utils::String::split(line, separation_character_.at(0));

    if (log_flag)
        ss << "Line " << line_cnt << ": Got " << parts.size() << " parts separated by '" << separation_character_.at(0) << "'" << std::endl;

    if (parts.size() < columns_.size())
    {
        ss << "Line " << line_cnt << ": ERROR: Got only " << parts.size() << " parts but " << columns_.size() << " defined columns " << std::endl;
        return false;
    }

    assert (hasAllColumns());

    unsigned int cnt=0;
    std::map <unsigned int, FormatDefinitionColumn*>::iterator it;
    std::string part; // where in six parts and every part a ducat
    FormatDefinitionColumn *col;
    FORMAT_DEFINITION_COLUMN_TYPE col_type;
    double value;
    bool ok;

    bool has_tod=false;
    double tod=0;
    bool has_latitude=false;
    double latitude=0;
    bool has_longitude=false;
    double longitude=0;
    bool has_altitude=false;
    double altitude=0;

    for (it = columns_.begin(); it != columns_.end(); it++)
    {
        assert (cnt < parts.size());
        col = it->second;

        part = parts.at(cnt);

        col_type = col->getType();

        assert (col_type != COLUMN_SENTINEL);

        if (col_type == COLUMN_LATITUDE || col_type == COLUMN_LONGITUDE || col_type == COLUMN_ALTITUDE)
        {
            value = Utils::String::doubleFromString(part, &ok);

            if (!ok)
            {
                ss << "Line " << line_cnt << ": ERROR: Could not convert part into floating point number" << std::endl;
                return false;
            }
        }

        if (col_type == COLUMN_TOD)
        {
            std::vector<std::string> time_parts = Utils::String::split(part, '.');

            if (time_parts.size() < 1 || time_parts.size() > 2)
            {
                ss << "Line " << line_cnt << ": ERROR 0: Could decode time into format (H)*HMMSS(.SS)*" << std::endl;
                return false;
            }

            std::string time_before_point = time_parts.at(0);
            std::string time_after_point;
            if (time_parts.size() == 2)
                time_after_point = time_parts.at(1);

            if (time_before_point.size() < 5 || time_before_point.size() > 6)
            {
                ss << "Line " << line_cnt << ": ERROR 1: Could decode time into format (H)*HMMSS(.SS)*" << std::endl;
                return false;
            }

            if (time_before_point.size() == 5) // leading zero correction
                time_before_point = "0"+time_before_point;

            std::string hours = time_before_point.substr(0,2);
            std::string minutes = time_before_point.substr(2,2);
            std::string seconds = time_before_point.substr(4,2);

            bool all_ok=true;
            bool ok;
            value = Utils::String::doubleFromString(hours, &ok) * 3600;
            all_ok |= ok;
            value += Utils::String::doubleFromString(minutes, &ok) * 60;
            all_ok |= ok;
            value += Utils::String::doubleFromString(seconds, &ok);
            all_ok |= ok;

            if (time_after_point.size() > 0)
            {
                value += Utils::String::doubleFromString(time_after_point, &ok) * 1 / (10*(double) time_after_point.size());
                all_ok |= ok;
            }

            if (!all_ok)
            {
                ss << "Line " << line_cnt << ": ERROR 2: Could decode time into format (H)*HMMSS(.SS)*" << std::endl;
                return false;
            }

            if (has_time_constant_)
            {
                if (log_flag)
                                ss << "Line " << line_cnt << ": Part " << cnt << ": Column " << col->getOrder() << ", " << col->getName()
                                << ": TOD has time constant " << time_constant_ << std::endl;
                value += time_constant_;
            }

            if (log_flag)
                ss << "Line " << line_cnt << ": Part " << cnt << ": Column " << col->getOrder() << ", " << col->getName()
                << ": TOD " << " part " << part << " formatted "
                << Utils::String::timeStringFromDouble(value) << std::endl;

            has_tod = true;
            tod = value;
        }
        else if (col_type == COLUMN_LATITUDE)
        {
            if (geo_coordinates_rad2deg_)
                value *= 360.0/(2*M_PI);

            if (log_flag)
                ss << "Line " << line_cnt << ": Part " << cnt << ": Column " << col->getOrder() << ", " << col->getName()
                << ": LAT " <<  Utils::String::doubleToStringPrecision(value, 4) << std::endl;

            has_latitude = true;
            latitude = value;

        }
        else if (col_type == COLUMN_LONGITUDE)
        {
            if (geo_coordinates_rad2deg_)
                value *= 360.0/(2*M_PI);

            if (log_flag)
                ss << "Line " << line_cnt << ": Part " << cnt << ": Column " << col->getOrder() << ", " << col->getName()
                << ": LONG " <<  Utils::String::doubleToStringPrecision(value, 4) << std::endl;

            has_longitude = true;
            longitude = value;
        }
        else if (col_type == COLUMN_ALTITUDE)
        {
            if (altitude_m2ft_)
                value *= 3.2808399;

            if (log_flag)
                ss << "Line " << line_cnt << ": Part " << cnt << ": Column " << col->getOrder() << ", " << col->getName()
                << ": ALT " <<  Utils::String::doubleToStringPrecision(value, 4) << std::endl;

            has_altitude = true;
            altitude = value;
        }
        else if (col_type == COLUMN_SKIP)
        {
            if (log_flag)
                ss << "Line " << line_cnt << ": Part " << cnt << ": SKIP" << std::endl;

            continue;
        }
        else if (col_type == COLUMN_STOP)
        {
            if (log_flag)
                ss << "Line " << line_cnt << ": Part " << cnt << ": STOP" << std::endl;

            break;
        }
        else
            assert (false); // the universe just broke. someone divided by 0 or crossed the beams.

        cnt ++;
    }

    assert (has_tod && has_latitude && has_longitude && has_altitude);

    if (trail)
        trail->addUpdate(tod, latitude, longitude, altitude);

    if (has_last_update_)
    {
        double time_diff = tod - last_tod;

        if (time_diff < 0)
            logwrn << "FormatDefinition: parse: time order of updates wrong, got time diff " << time_diff << " at line " << line_cnt;

        if (time_diff > 4.0)
            logwrn << "FormatDefinition: parse: time gap between updates wrong, got time diff " << time_diff << " at line " << line_cnt;
    }

    has_last_update_ = true;
    last_tod = tod;
    last_latitude = latitude;
    last_longitude= longitude;
    last_altitude= altitude;

    return true;
}

bool FormatDefinition::hasAllColumns ()
{
    bool has_tod_=false;
    bool has_latitude_=false;
    bool has_longitude_=false;
    bool has_altitude_=false;

    std::map <unsigned int, FormatDefinitionColumn*>::iterator it;
    FORMAT_DEFINITION_COLUMN_TYPE col_type;

    for (it = columns_.begin(); it != columns_.end(); it++)
    {
        col_type = it->second->getType();
        assert (col_type != COLUMN_SENTINEL);

        if (col_type == COLUMN_TOD)
            has_tod_=true;
        else if (col_type == COLUMN_LATITUDE)
            has_latitude_=true;
        else if (col_type == COLUMN_LONGITUDE)
            has_longitude_=true;
        else if (col_type == COLUMN_ALTITUDE)
            has_altitude_=true;
        else if (col_type == COLUMN_STOP)
            ; // not suspicious at all - move along sir
        else
            assert (false); // the universe just broke. someone divided by 0 or crossed the beams.
    }

    return (has_tod_ && has_latitude_ && has_longitude_ && has_altitude_);
}

}
