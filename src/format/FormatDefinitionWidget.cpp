/*
 * FormatDefinitionWidget.cpp
 *
 *  Created on: Jun 26, 2014
 *      Author: sk
 */

#include "FormatDefinitionWidget.h"
#include "FormatDefinition.h"
#include "FormatDefinitionColumn.h"
#include "String.h"

#include <QVBoxLayout>
#include <QLabel>
#include <QTableWidget>
#include <QTableWidgetItem>
#include <QLineEdit>
#include <QGridLayout>
#include <QPushButton>
#include <QMessageBox>
#include <QInputDialog>
#include <QCheckBox>

using namespace Utils::String;

namespace MFImport
{

FormatDefinitionWidget::FormatDefinitionWidget(QWidget* parent, Qt::WindowFlags f)
: QWidget (parent, f), current_(0), name_edit_(0), separation_char_edit_(0), comment_char_edit_(0),
  geo_coordinates_rad2deg_check_(0), altitude_m2ft_check_(0), time_constant_check_(0), time_constant_edit_(0),
  update_button_(0), columns_table_(0), add_button_(0)
{
    createGUIElements ();
}

FormatDefinitionWidget::~FormatDefinitionWidget()
{
    if (columns_table_)
    {
        delete columns_table_;
        columns_table_=0;
    }

}

void FormatDefinitionWidget::createGUIElements ()
{
    QFont font_bold;
    font_bold.setBold(true);

    QVBoxLayout *layout = new QVBoxLayout ();

    QLabel *label = new QLabel ("Format Definition");
    label->setFont (font_bold);
    layout->addWidget(label);

    QGridLayout *glayout = new QGridLayout ();

    QLabel *flabel = new QLabel ("Name");
    glayout->addWidget (flabel, 0, 0);

    name_edit_ = new QLineEdit ("");
    glayout->addWidget (name_edit_, 0, 1);

    QLabel *char_label = new QLabel ("Separation Character");
    glayout->addWidget (char_label, 1, 0);

    separation_char_edit_ = new QLineEdit ();
    glayout->addWidget (separation_char_edit_, 1, 1);

    QLabel *com_label = new QLabel ("Comment Character");
    glayout->addWidget (com_label, 2, 0);

    comment_char_edit_ = new QLineEdit ();
    glayout->addWidget (comment_char_edit_, 2, 1);

//    QLabel *geolabel = new QLabel ("Convert Geo Coordinates Radian->Degree");
//    glayout->addWidget (geolabel, 3, 0);

    geo_coordinates_rad2deg_check_ = new QCheckBox ("Convert Geo Coordinates Radian->Degree");
    glayout->addWidget (geo_coordinates_rad2deg_check_, 3, 0);

//    QLabel *altlabel = new QLabel ("Convert Altitude Meter->Feet");
//    glayout->addWidget (altlabel, 4, 0);

    altitude_m2ft_check_ = new QCheckBox ("Convert Altitude Meter->Feet");
    glayout->addWidget (altitude_m2ft_check_, 4, 0);

    time_constant_check_ = new QCheckBox ("Add Time Constant (UTC=GPS-16s after 2012)");
    glayout->addWidget (time_constant_check_, 5, 0);

    time_constant_edit_ = new QLineEdit ("0.0");
    glayout->addWidget (time_constant_edit_, 5, 1);

    layout->addLayout (glayout);

    update_button_ = new QPushButton ("Update");
    connect (update_button_, SIGNAL (clicked()), this, SLOT(update()));
    update_button_->setDisabled(true);
    layout->addWidget (update_button_);

    QLabel *collabel = new QLabel ("Columns");
    collabel->setFont(font_bold);
    layout->addWidget (collabel);

    columns_table_ = new QTableWidget ();
    columns_table_->setSelectionMode(QAbstractItemView::NoSelection);

    QStringList header_list;
    header_list.append (tr("Name"));
    header_list.append (tr("Type"));
    header_list.append (tr("Up"));
    header_list.append (tr("Down"));
    header_list.append (tr("Delete"));
    columns_table_->setColumnCount(header_list.size());
    columns_table_->setHorizontalHeaderLabels (header_list);

    layout->addWidget (columns_table_);

    add_button_ = new QPushButton ("New Column");
    connect (add_button_, SIGNAL (clicked()), this, SLOT(addColumn()));
    add_button_->setDisabled(true);
    layout->addWidget (add_button_);

    setLayout (layout);
}

void FormatDefinitionWidget::show (FormatDefinition *format)
{
    current_ = format;

    assert (name_edit_);
    name_edit_->setText ("");
    assert (separation_char_edit_);
    separation_char_edit_->setText ("");
    assert (comment_char_edit_);
    comment_char_edit_->setText ("");

    if (current_)
    {
        name_edit_->setText (current_->getName().c_str());
        separation_char_edit_->setText (current_->getSeparationCharacter().c_str());
        comment_char_edit_->setText (current_->getCommentCharacter().c_str());
        geo_coordinates_rad2deg_check_->setChecked(current_->getGeoCoordinatesRad2deg());
        altitude_m2ft_check_->setChecked(current_->getAltitudeM2ft());
        time_constant_check_->setChecked(current_->getHasTimeConstant());
        time_constant_edit_->setText (doubleToString(current_->getTimeConstant()).c_str());

        updateColumnsTable();

        update_button_->setDisabled(false);
        add_button_->setDisabled(false);
    }
    else
    {
        name_edit_->setText ("");
        separation_char_edit_->setText ("");
        comment_char_edit_->setText ("");
        geo_coordinates_rad2deg_check_->setChecked(false);
        altitude_m2ft_check_->setChecked(false);
        time_constant_check_->setChecked(false);
        time_constant_edit_->setText ("0.0");

        update_button_->setDisabled(true);
        add_button_->setDisabled(true);

        assert (columns_table_);

        columns_table_->clearContents();
        name_items_.clear();
        type_boxes_.clear();
        up_buttons_.clear();
        down_buttons_.clear();
        delete_buttons_.clear();
    }
}

void FormatDefinitionWidget::updateColumnsTable()
{
    assert (columns_table_);
    assert (current_);

    columns_table_->clearContents();
    name_items_.clear();
    type_boxes_.clear();
    up_buttons_.clear();
    down_buttons_.clear();
    delete_buttons_.clear();

    const std::map <unsigned int, FormatDefinitionColumn*> &cols = current_->getColumns ();
    std::map <unsigned int, FormatDefinitionColumn*>::const_iterator it;

    columns_table_->setRowCount(cols.size());

    unsigned int row = 0;
    //QTableWidgetItem *newItem=0;
    FormatDefinitionColumn *col;

    QPixmap* pixmapup = new QPixmap("icons/collapse.png");
    QPixmap* pixmapdown = new QPixmap("icons/expand.png");
    QPixmap* pixmapdel = new QPixmap("icons/close_icon.png");

    int column;
    for (it = cols.begin(); it != cols.end(); it++)
    {
        col = it->second;
        column=0;

        QLineEdit *name_edit = new QLineEdit(col->getName().c_str());
        name_items_[name_edit] = col->getName();
        name_edit->setMinimumWidth(200);
        columns_table_->setCellWidget(row, column, name_edit);
        connect (name_edit, SIGNAL(returnPressed()), this, SLOT(nameChanged()));
        column++;

        FormatDefinitionColumnTypeComboBox *box = new FormatDefinitionColumnTypeComboBox ();
        box->setType(col->getType());
        columns_table_->setCellWidget(row, column, box);
        type_boxes_[box] = it->second->getName();
        connect (box, SIGNAL(currentIndexChanged (const QString &)), this, SLOT(typeChanged (const QString &)));
        column++;

        QPushButton *up = new QPushButton ();
        up->setIcon(QIcon(*pixmapup));
        up->setFlat(true);
        up->setToolTip(tr("Move column up"));
        columns_table_->setCellWidget(row, column, up);
        up_buttons_[up] = it->second->getName();
        connect( up, SIGNAL(clicked()), this, SLOT(moveColumnUp()) );
        column++;

        QPushButton *down = new QPushButton ();
        down->setIcon(QIcon(*pixmapdown));
        down->setFlat(true);
        down->setToolTip(tr("Move column down"));
        columns_table_->setCellWidget(row, column, down);
        down_buttons_[down] = it->second->getName();
        connect( down, SIGNAL(clicked()), this, SLOT(moveColumnDown()) );
        column++;

        QPushButton *del = new QPushButton ();
        del->setIcon(QIcon(*pixmapdel));
        del->setFlat(true);
        del->setToolTip(tr("Delete column"));
        columns_table_->setCellWidget(row, column, del);
        delete_buttons_[del] = it->second->getName();
        connect (del, SIGNAL(clicked()), this, SLOT(deleteColumn()));
        column++;

        row++;
    }

    columns_table_->resizeColumnsToContents();
}

void FormatDefinitionWidget::update ()
{
    if (!current_)
    {
        logwrn << "FormatDefinitionWidget: update: no current format definition set";
        return;
    }

    assert (name_edit_);
    assert (separation_char_edit_);
    assert (geo_coordinates_rad2deg_check_);
    assert (altitude_m2ft_check_);
    assert (time_constant_edit_);

    if (current_->getName() != name_edit_->text().toStdString())
    {
        current_->setName (name_edit_->text().toStdString());
    }

    if (separation_char_edit_->text().size() != 1)
    {
        QMessageBox::warning ( this, "Update Format Definition Error", "Separation Character can only be of length 1");
        return;
    }

    if (comment_char_edit_->text().size() != 1)
    {
        QMessageBox::warning ( this, "Update Format Definition Error", "Comment Character can only be of length 1");
        return;
    }

    current_->setSeparationCharacter(separation_char_edit_->text().at(0).toAscii());
    current_->setCommentCharacter(comment_char_edit_->text().at(0).toAscii());

    current_->setGeoCoordinatesRad2deg(geo_coordinates_rad2deg_check_->checkState() == Qt::Checked);
    current_->setAltitudeM2ft(altitude_m2ft_check_->checkState() == Qt::Checked);
    if (time_constant_check_->checkState() == Qt::Checked)
        current_->setTimeConstant(doubleFromString(time_constant_edit_->text().toStdString()));

    emit updated();
}

void FormatDefinitionWidget::addColumn ()
{
    assert (current_);

    bool ok;
    QString text = QInputDialog::getText(this, tr("Add Format Definition Column"), tr("Enter Format Name (must be unique):"), QLineEdit::Normal,
            "NewFormatColumn", &ok);

    if (ok && !text.isEmpty())
    {
        std::string name = text.toStdString();
        loginf << "FormatDefinitionWidget: addColumn: " << name;

        if (current_->hasColumn(name))
        {
            QMessageBox::warning ( this, "Add Format Definition Column Error", "Format Definition Column with name "+text+" already exists");
            return;
        }

        Configuration &config = current_->addNewSubConfiguration("FormatDefinitionColumn");
        config.addParameterString("name", name);
        config.addParameterInt("order", current_->getMaxOrder());
        current_->generateSubConfigurable(config.getClassId(), config.getInstanceId());
        emit updated();
    }
}

void FormatDefinitionWidget::deleteColumn ()
{
    assert (current_);

    QPushButton *button = dynamic_cast<QPushButton*> (QObject::sender());
    assert (button);
    assert (delete_buttons_.find(button) != delete_buttons_.end());

    std::string name = delete_buttons_[button];
    assert (current_->hasColumn(name));
    current_->deleteColumn(name);

    loginf << "FormatDefinitionWidget: deleteColumn: deleted column " << name;

    updateColumnsTable();
}
void FormatDefinitionWidget::moveColumnUp()
{
    assert (current_);

    QPushButton *button = dynamic_cast<QPushButton*> (QObject::sender());
    assert (button);
    assert (up_buttons_.find(button) != up_buttons_.end());

    std::string name = up_buttons_[button];
    assert (current_->hasColumn(name));
    current_->moveColumnUp(name);

    //loginf << "FormatDefinitionWidget: moveColumnUp: moved column " << name;

    updateColumnsTable();
}
void FormatDefinitionWidget::moveColumnDown()
{
    assert (current_);

    QPushButton *button = dynamic_cast<QPushButton*> (QObject::sender());
    assert (button);
    assert (down_buttons_.find(button) != down_buttons_.end());

    std::string name = down_buttons_[button];
    assert (current_->hasColumn(name));
    current_->moveColumnDown(name);

    //loginf << "FormatDefinitionWidget: moveColumnDown: moved column " << name;

    updateColumnsTable();
}


void FormatDefinitionWidget::nameChanged ()
{
    assert (current_);

    QLineEdit *edit = dynamic_cast<QLineEdit*> (QObject::sender());
    assert (edit);
    assert (name_items_.find (edit) != name_items_.end());

    std::string name = name_items_[edit];
    assert (current_->hasColumn(name));

    FormatDefinitionColumn *col = current_->getColumn(name);

    col->setName(edit->text().toStdString());

    loginf << "FormatDefinitionWidget: itemChanged: column name changed to " << col->getName();

    updateColumnsTable ();

}

void FormatDefinitionWidget::typeChanged (const QString &text)
{
    assert (current_);

    FormatDefinitionColumnTypeComboBox *box = dynamic_cast<FormatDefinitionColumnTypeComboBox*> (QObject::sender());
    assert (box);
    assert (type_boxes_.find (box) != type_boxes_.end());

    std::string name = type_boxes_[box];
    assert (current_->hasColumn(name));

    FormatDefinitionColumn *col = current_->getColumn(name);

    col->setType(box->getType());

    loginf << "FormatDefinitionWidget: typeChanged: column " << col->getName() << " changed type to "
            << FORMAT_DEFINITION_COLUMN_TYPE_STRINGS.at(box->getType());
}

} /* namespace MFImport */
