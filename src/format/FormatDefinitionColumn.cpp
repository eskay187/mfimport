/*
 * FormatDefinitionColumn.cpp
 *
 *  Created on: Jun 25, 2014
 *      Author: sk
 */

#include "FormatDefinitionColumn.h"
#include "GPSTrail.h"

#include <boost/assign/list_of.hpp>

namespace MFImport
{

std::map<FORMAT_DEFINITION_COLUMN_TYPE,std::string> FORMAT_DEFINITION_COLUMN_TYPE_STRINGS = boost::assign::map_list_of
(COLUMN_TOD,     "COLUMN_TOD"    )
(COLUMN_LATITUDE,     "COLUMN_LATITUDE"    )
(COLUMN_LONGITUDE, "COLUMN_LONGITUDE")
(COLUMN_ALTITUDE, "COLUMN_ALTITUDE")
(COLUMN_SKIP, "COLUMN_SKIP")
(COLUMN_STOP, "COLUMN_STOP")
(COLUMN_SENTINEL, "COLUMN_SENTINEL");

FormatDefinitionColumn::FormatDefinitionColumn(std::string class_id, std::string instance_id, Configurable *parent)
: Configurable (class_id, instance_id, parent)
{
    registerParameter ("order", &order_, -1);
    registerParameter ("name", &name_, "");
    registerParameter ("type_int", &type_int_, (unsigned char) COLUMN_SKIP);

    assert (order_ != -1);
    assert (name_.size() != 0);
    assert (type_int_ != COLUMN_SENTINEL);
}

FormatDefinitionColumn::~FormatDefinitionColumn()
{
}

void FormatDefinitionColumn::generateSubConfigurable (std::string class_id, std::string instance_id)
{
    throw std::runtime_error ("FormatDefinitionColumn: generateSubConfigurable: unknown class_id "+class_id);
}

void FormatDefinitionColumn::parse (std::string item_string, GPSTrail *trail, bool &stop)
{
    //TODO
}

} /* namespace MFImport */
