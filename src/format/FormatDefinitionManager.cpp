/*
 * FormatDefinitionManager.cpp
 *
 *  Created on: Jun 26, 2014
 *      Author: sk
 */

#include "FormatDefinitionManager.h"
#include "FormatDefinition.h"

namespace MFImport
{

FormatDefinitionManager::FormatDefinitionManager()
: Singleton(), Configurable ("FormatDefinitionManager", "FormatDefinitionManager0", 0, "conf/config_gpstrail_formats.xml")
{
    registerParameter ("current_format", &current_format_, "");

    createSubConfigurables();
}

FormatDefinitionManager::~FormatDefinitionManager()
{
    std::map <std::string, FormatDefinition*>::iterator it;

    for (it = formats_.begin(); it != formats_.end(); it++)
        delete it->second;
    formats_.clear();

}

void FormatDefinitionManager::generateSubConfigurable (std::string class_id, std::string instance_id)
{
    if (class_id == "FormatDefinition")
    {
        FormatDefinition *format = new FormatDefinition (class_id, instance_id, this);
        if (formats_.find (format->getName()) != formats_.end())
        {
            logerr << "FormatDefinitionManager: generateSubConfigurable: format definition " << format->getName() << " already exists, deleting";
            delete format;
            return;
        }
        formats_ [format->getName()]=format;
    }
    else
        throw std::runtime_error ("FormatDefinitionManager: generateSubConfigurable: unknown class_id "+class_id);
}

void FormatDefinitionManager::deleteCurrentFormat ()
{
    assert (hasCurrentFormat());

    delete formats_[current_format_];
    formats_.erase(formats_.find(current_format_));
    current_format_="";
}

void FormatDefinitionManager::updateFormats ()
{
    std::map <std::string, FormatDefinition*> old = formats_;
    formats_.clear();

    std::map <std::string, FormatDefinition*>::iterator it;

    for (it = old.begin(); it != old.end(); it++)
    {
        if (formats_.find (it->second->getName()) != formats_.end())
        {
            logwrn << "FormatDefinitionManager: updateFormats: format with name "<< it->second->getName() << " already exists, removing";
            delete it->second;
            continue;
        }
        formats_ [it->second->getName()] = it->second;
    }
}

} /* namespace MFImport */
