/*
 * FormatDefinitionManagerWidget.h
 *
 *  Created on: Jun 26, 2014
 *      Author: sk
 */

#ifndef FORMATDEFINITIONMANAGERWIDGET_H_
#define FORMATDEFINITIONMANAGERWIDGET_H_

#include <QWidget>

class QListWidget;

namespace MFImport
{
class FormatDefinitionWidget;

class FormatDefinitionManagerWidget : public QWidget
{
    Q_OBJECT
public slots:
    void addFormat ();
    void deleteSelectedFormat ();
    void formatSelectionChanged ();
    void formatUpdated ();

public:
    FormatDefinitionManagerWidget(QWidget* parent = 0, Qt::WindowFlags f = 0);
    virtual ~FormatDefinitionManagerWidget();

protected:
    QListWidget *format_list_;
    FormatDefinitionWidget *format_widget_;

    void createGUIElements ();
    void updateFormatList ();
};

} /* namespace MFImport */

#endif /* FORMATDEFINITIONMANAGERWIDGET_H_ */
