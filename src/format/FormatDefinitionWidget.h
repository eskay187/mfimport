/*
 * FormatDefinitionWidget.h
 *
 *  Created on: Jun 26, 2014
 *      Author: sk
 */

#ifndef FORMATDEFINITIONWIDGET_H_
#define FORMATDEFINITIONWIDGET_H_

#include <QWidget>
#include <QComboBox>
#include "FormatDefinitionColumn.h"

class QTableWidget;
class QTableWidgetItem;
class QLineEdit;
class QPushButton;
class QCheckBox;

namespace MFImport
{

class FormatDefinition;


class FormatDefinitionColumnTypeComboBox : public QComboBox
{
    Q_OBJECT
public:
    FormatDefinitionColumnTypeComboBox (QWidget *parent = 0)
    : QComboBox (parent)
    {
        std::map<FORMAT_DEFINITION_COLUMN_TYPE,std::string>::iterator it;

        for (it = FORMAT_DEFINITION_COLUMN_TYPE_STRINGS.begin(); it != FORMAT_DEFINITION_COLUMN_TYPE_STRINGS.end(); it++)
        {
            if (it->first == COLUMN_SENTINEL)
                continue;

            addItem(it->second.c_str(), (int)it->first);
        }
    }
    virtual ~FormatDefinitionColumnTypeComboBox ()
    {

    }

    FORMAT_DEFINITION_COLUMN_TYPE getType ()
    {
        bool ok;
        int data = itemData(currentIndex()).toInt(&ok);
        assert (ok);
        assert (data >= 0 && FORMAT_DEFINITION_COLUMN_TYPE_STRINGS.find((FORMAT_DEFINITION_COLUMN_TYPE) data)
                != FORMAT_DEFINITION_COLUMN_TYPE_STRINGS.end());

        return (FORMAT_DEFINITION_COLUMN_TYPE) data;
    }

    void setType (FORMAT_DEFINITION_COLUMN_TYPE type)
    {
        int index = findText(FORMAT_DEFINITION_COLUMN_TYPE_STRINGS.at(type).c_str());
        assert (index >= 0);
        setCurrentIndex(index);
    }
};

class FormatDefinitionWidget : public QWidget
{
    Q_OBJECT
public slots:
    void update ();
    void addColumn();
    void deleteColumn ();
    void moveColumnUp();
    void moveColumnDown();
    void nameChanged ();
    void typeChanged (const QString &text);

signals:
    void updated ();

public:
    FormatDefinitionWidget(QWidget* parent = 0, Qt::WindowFlags f = 0);
    virtual ~FormatDefinitionWidget();

    void show (FormatDefinition *format); // may be 0 if show none

protected:
    FormatDefinition *current_;
    QLineEdit *name_edit_;
    QLineEdit *separation_char_edit_;
    QLineEdit *comment_char_edit_;
    QCheckBox *geo_coordinates_rad2deg_check_;
    QCheckBox *altitude_m2ft_check_;
    QCheckBox *time_constant_check_;
    QLineEdit *time_constant_edit_;
    QPushButton *update_button_;
    QTableWidget *columns_table_;
    QPushButton *add_button_;

    std::map <QLineEdit *, std::string> name_items_;
    std::map <FormatDefinitionColumnTypeComboBox *, std::string> type_boxes_;
    std::map <QPushButton *, std::string> up_buttons_;
    std::map <QPushButton *, std::string> down_buttons_;
    std::map <QPushButton *, std::string> delete_buttons_;

    void updateColumnsTable();
    void createGUIElements ();
};

} /* namespace MFImport */

#endif /* FORMATDEFINITIONWIDGET_H_ */
