/*
 * FormatDefinitionManagerWidget.cpp
 *
 *  Created on: Jun 26, 2014
 *      Author: sk
 */

#include "FormatDefinitionManagerWidget.h"
#include "FormatDefinitionManager.h"
#include "FormatDefinition.h"
#include "FormatDefinitionWidget.h"

#include <QVBoxLayout>
#include <QLabel>
#include <QPushButton>
#include <QFileDialog>
#include <QTextEdit>
#include <QListWidget>
#include <QListWidgetItem>
#include <QInputDialog>
#include <QMessageBox>

namespace MFImport
{

FormatDefinitionManagerWidget::FormatDefinitionManagerWidget(QWidget* parent, Qt::WindowFlags f)
: QWidget (parent, f), format_list_(0), format_widget_(0)
{
    createGUIElements();
}

FormatDefinitionManagerWidget::~FormatDefinitionManagerWidget()
{
    if (format_list_)
    {
        delete format_list_;
        format_list_=0;
    }
    if (format_widget_)
    {
        delete format_widget_;
        format_widget_=0;
    }
}

void FormatDefinitionManagerWidget::createGUIElements ()
{
    QFont font_bold;
    font_bold.setBold(true);

    QVBoxLayout *layout = new QVBoxLayout ();

    QHBoxLayout *hlayout = new QHBoxLayout ();

    QVBoxLayout *list_layout = new QVBoxLayout ();

    QLabel *list_label = new QLabel ("Active Format Selection");
    list_label->setFont (font_bold);
    list_layout->addWidget (list_label);

    format_list_ = new QListWidget();
    updateFormatList();
    connect (format_list_, SIGNAL(itemSelectionChanged ()), this, SLOT(formatSelectionChanged()));
    format_list_->setMaximumWidth(300);
    list_layout->addWidget (format_list_);

    QPushButton *add_format = new QPushButton ("New Format");
    connect (add_format, SIGNAL(clicked()), this, SLOT(addFormat()));
    list_layout->addWidget (add_format);

    QPushButton *del_format = new QPushButton ("Delete Selected Format");
    connect (del_format, SIGNAL(clicked()), this, SLOT(deleteSelectedFormat()));
    list_layout->addWidget (del_format);

    hlayout->addLayout (list_layout);

    format_widget_ = new FormatDefinitionWidget();
    connect (format_widget_, SIGNAL(updated()), this, SLOT(formatUpdated()));

    if (FormatDefinitionManager::getInstance().hasCurrentFormat())
        format_widget_->show(FormatDefinitionManager::getInstance().getCurrentFormat());

    hlayout->addWidget(format_widget_);

    layout->addLayout (hlayout);

    setLayout (layout);
}

void FormatDefinitionManagerWidget::updateFormatList ()
{
    assert (format_list_);

    format_list_->clear();

    FormatDefinition *current=0;

    if (FormatDefinitionManager::getInstance().hasCurrentFormat())
        current = FormatDefinitionManager::getInstance().getCurrentFormat();

    const std::map <std::string, FormatDefinition*> &formats = FormatDefinitionManager::getInstance().getFormats();
    std::map <std::string, FormatDefinition*>::const_iterator it;

    QListWidgetItem *item=0;

    for (it = formats.begin(); it !=formats.end(); it++)
    {
        item = new QListWidgetItem(tr(it->second->getName().c_str()), format_list_);

        if (current && current == it->second)
            format_list_->setItemSelected(item, true);
    }
}

void FormatDefinitionManagerWidget::formatSelectionChanged ()
{
    assert (format_list_);
    assert (format_widget_);

    QList<QListWidgetItem *> selection = format_list_->selectedItems ();
    QList<QListWidgetItem *>::iterator it;

    std::string name;
    for (it = selection.begin(); it != selection.end(); it++)
    {
        name = (*it)->text().toStdString();
        loginf << "FormatDefinitionManagerWidget: formatSelectionChanged: selected format " << name;
        assert (FormatDefinitionManager::getInstance().hasFormat(name));

        FormatDefinitionManager::getInstance().setCurrentFormat(name);
        assert (FormatDefinitionManager::getInstance().hasCurrentFormat());
        format_widget_->show(FormatDefinitionManager::getInstance().getCurrentFormat());
    }

    if (selection.size() == 0)
        format_widget_->show(0);
}

void FormatDefinitionManagerWidget::addFormat ()
{
    bool ok;
    QString text = QInputDialog::getText(this, tr("Add Format Definition"), tr("Enter Format Name (must be unique):"), QLineEdit::Normal,
            "NewFormat", &ok);

    if (ok && !text.isEmpty())
    {
        std::string name = text.toStdString();
        loginf << "FormatDefinitionManagerWidget: addFormat: " << name;

        if (FormatDefinitionManager::getInstance().hasFormat(name))
        {
            QMessageBox::warning ( this, "Add Format Definition Error", "FormatDefinition with name "+text+" already exists");
            return;
        }

        Configuration &config = FormatDefinitionManager::getInstance().addNewSubConfiguration("FormatDefinition");
        config.addParameterString("name", name);
        FormatDefinitionManager::getInstance().generateSubConfigurable(config.getClassId(), config.getInstanceId());

        updateFormatList();
    }
}

void FormatDefinitionManagerWidget::deleteSelectedFormat ()
{
    if (!FormatDefinitionManager::getInstance().hasCurrentFormat())
    {
        QMessageBox::warning ( this, "Delete Format Definition", "Not possible, select format definition to delete");
        return;
    }

    FormatDefinition *current = FormatDefinitionManager::getInstance().getCurrentFormat();
    std::string name = current->getName();

    QMessageBox msgBox;
    msgBox.setWindowTitle("Delete Format Definition");
    msgBox.setText(tr("Delete ")+name.c_str()+tr("?"));
    msgBox.setStandardButtons(QMessageBox::Ok | QMessageBox::Cancel);
    msgBox.setDefaultButton(QMessageBox::Ok);
    int ret = msgBox.exec();

    if (ret == QMessageBox::Cancel)
        return;

    if (ret == QMessageBox::Ok)
    {
        loginf << "FormatDefinitionManagerWidget: deleteSelectedFormat: " << name;
        FormatDefinitionManager::getInstance().deleteCurrentFormat ();
        updateFormatList();
        format_widget_->show(0);
    }

}

void FormatDefinitionManagerWidget::formatUpdated ()
{
    updateFormatList();
}

} /* namespace MFImport */
