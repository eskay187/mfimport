/*
 * FormatDefinitionManager.h
 *
 *  Created on: Jun 26, 2014
 *      Author: sk
 */

#ifndef FORMATDEFINITIONMANAGER_H_
#define FORMATDEFINITIONMANAGER_H_

#include "Singleton.h"
#include "Configurable.h"

namespace MFImport
{
class FormatDefinition;

class FormatDefinitionManager : public Singleton, public Configurable
{
public:
    static FormatDefinitionManager& getInstance()
    {
      static FormatDefinitionManager instance;
      return instance;
    }

    virtual ~FormatDefinitionManager();

    virtual void generateSubConfigurable (std::string class_id, std::string instance_id);

    const std::map <std::string, FormatDefinition*> &getFormats () { return formats_; }

    bool hasFormat (std::string name) { return formats_.find (name) != formats_.end(); }

    bool hasCurrentFormat () { return hasFormat(current_format_);}
    FormatDefinition *getCurrentFormat () { assert (hasFormat(current_format_)); return formats_[current_format_]; }
    void setCurrentFormat (std::string name) { assert (hasFormat(name)); current_format_=name; }

    void deleteCurrentFormat ();
    void updateFormats ();

protected:
    std::string current_format_;
    std::map <std::string, FormatDefinition*> formats_;

    FormatDefinitionManager();
    virtual void checkSubConfigurables () {}
};

} /* namespace MFImport */

#endif /* FORMATDEFINITIONMANAGER_H_ */
