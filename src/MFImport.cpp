/*
 * MFImport.cpp
 *
 *  Created on: Jul 30, 2009
 *      Author: sk
 */
#include <QApplication>
#include "Logger.h"
#include "MFImport.h"

#include <QMessageBox>

using namespace std;

namespace MFImport
{

MFImport::MFImport(int& argc, char ** argv)
 : QApplication(argc, argv)
{
}

bool MFImport::notify(QObject * receiver, QEvent * event)
{
  try
  {
    return QApplication::notify(receiver, event);
  }
  catch(std::exception& e)
  {
    logerr  << "MFImport: Exception thrown: " << e.what();
    QMessageBox::critical( NULL, "MFImport::notify(): Exception", QString( e.what() ) );
  }
  catch(...)
  {
    QMessageBox::critical( NULL, "MFImport::notify(): Exception", "Unknown exception" );
  }
  return false;
}
}
