/*
 * ImportFileWidget.h
 *
 *  Created on: Jun 26, 2014
 *      Author: sk
 */

#ifndef IMPORTFILEWIDGET_H_
#define IMPORTFILEWIDGET_H_

#include <cassert>
#include <QWidget>

class QLabel;
class QTextEdit;

namespace MFImport
{

class ImportFileWidget : public QWidget
{
    Q_OBJECT
public slots:
    void selectInputFile ();

public:
    ImportFileWidget(QWidget* parent = 0, Qt::WindowFlags f = 0);
    virtual ~ImportFileWidget();

//    bool hasFilename () { return has_filename_; }
//    std::string getFilename () { assert (has_filename_); return filename_; }

protected:
//    bool has_filename_;
//    std::string filename_;

    QLabel *filename_label_;
    QTextEdit *file_contents_;

    void createGUIElements ();
    void showFileContents (std::string filename);
};

} /* namespace MFImport */

#endif /* IMPORTFILEWIDGET_H_ */
