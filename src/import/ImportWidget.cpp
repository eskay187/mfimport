/*
 * ImportWidget.cpp
 *
 *  Created on: Jun 26, 2014
 *      Author: sk
 */

#include "ImportWidget.h"
#include "ImportFileWidget.h"
#include "ImportTrailWidget.h"
#include "FormatDefinitionManagerWidget.h"
#include "Logger.h"

#include <QVBoxLayout>
#include <QLabel>
#include <QTabWidget>

namespace MFImport
{

ImportWidget::ImportWidget(QWidget* parent, Qt::WindowFlags f)
: QWidget (parent, f), file_widget_(0), format_widget_(0), import_widget_(0)
{
    createGUIElements ();
    setMinimumSize(QSize(1000, 800));
    show();
}

ImportWidget::~ImportWidget()
{
    if (file_widget_)
    {
        delete file_widget_;
        file_widget_=0;
    }

    if (format_widget_)
    {
        delete format_widget_;
        format_widget_=0;
    }

    if (import_widget_)
    {
        delete import_widget_;
        import_widget_=0;
    }
}

void ImportWidget::createGUIElements ()
{
    QFont font_bold;
    font_bold.setBold(true);

    QFont font_big;
    font_big.setPointSize(18);

    QVBoxLayout *layout = new QVBoxLayout ();

    QLabel *label = new QLabel ("GPS Trail Import");
    label->setFont(font_big);
    layout->addWidget (label);

    QTabWidget *tab_widget = new QTabWidget ();

    file_widget_ = new ImportFileWidget ();
    tab_widget->addTab(file_widget_, "Input File");

    format_widget_ = new FormatDefinitionManagerWidget ();
    tab_widget->addTab(format_widget_, "Format Definition");

    import_widget_ = new ImportTrailWidget (file_widget_);
    tab_widget->addTab(import_widget_, "Import");

    layout->addWidget (tab_widget);

    setLayout (layout);
}



} /* namespace MFImport */
