/*
 * ImportTrailWidget.cpp
 *
 *  Created on: Jul 1, 2014
 *      Author: sk
 */

#include "ImportTrailWidget.h"
#include "ImportFileWidget.h"
#include "GPSTrailManager.h"
#include "FormatDefinitionManager.h"
#include "FormatDefinition.h"
#include "Logger.h"

#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QPushButton>
#include <QTextEdit>
#include <QLabel>
#include <QMessageBox>
#include <QLineEdit>
#include <QInputDialog>

namespace MFImport
{

ImportTrailWidget::ImportTrailWidget(ImportFileWidget *file_widget, QWidget* parent, Qt::WindowFlags f)
: QWidget (parent, f), file_widget_(file_widget), test_button_ (0), test_success_(false), import_button_(0), log_edit_(0)
{
    createGUIElements();
}

ImportTrailWidget::~ImportTrailWidget()
{
}

void ImportTrailWidget::createGUIElements ()
{
    QFont font_bold;
    font_bold.setBold(true);

    QVBoxLayout *layout = new QVBoxLayout ();

    QLabel *label = new QLabel ("Perform Test / Import");
    label->setFont(font_bold);
    layout->addWidget (label);

    QHBoxLayout *blayout = new QHBoxLayout ();

    test_button_ = new QPushButton ("Test");
    test_button_->setToolTip("Test file import with the selected Format Definition.");
    connect (test_button_, SIGNAL(clicked()), this, SLOT(test()));
    blayout->addWidget (test_button_);

    import_button_ = new QPushButton ("Import");
    import_button_->setToolTip("Import with the selected Format Definition.\n Can only be performed after successful test.");
    import_button_->setDisabled(true);
    connect (import_button_, SIGNAL(clicked()), this, SLOT(import()));
    blayout->addWidget (import_button_);

    blayout->addStretch();
    layout->addLayout (blayout);

    QLabel *loglabel = new QLabel ("Output Log");
    loglabel->setFont(font_bold);
    layout->addWidget (loglabel);

    log_edit_ = new QTextEdit ();
    layout->addWidget (log_edit_);

    setLayout (layout);
}

void ImportTrailWidget::test ()
{
    assert (!test_success_);
    assert (file_widget_);

    log_edit_->clear();

    if (!GPSTrailManager::getInstance().hasTrailFilename())
    {
        QMessageBox::warning ( this, "Test Import Trail", "No Input File selected:\n Please use to the 'Input File' tab to do so.");
        return;
    }

    if (!FormatDefinitionManager::getInstance().hasCurrentFormat())
    {
        QMessageBox::warning ( this, "Test Import Trail", "No Format Definition selected:\n Please use to the 'Format Definition' tab to do so.");
        return;
    }

    if (!FormatDefinitionManager::getInstance().getCurrentFormat()->hasAllColumns())
    {
        QMessageBox::warning ( this, "Test Import Trail", "Format Definition does not have all required columns:\n"
                " Please use to the 'Format Definition' tab to do so.");
        return;
    }

    std::stringstream ss;

    bool ret = GPSTrailManager::getInstance().importTrail(ss, false, "", true);

    log_edit_->setPlainText(ss.str().c_str());

    if (ret)
    {
        loginf << "ImportTrailWidget: test: success";
        test_button_->setDisabled(true);
        import_button_->setDisabled(false);
        test_success_=true;
    }
    else
        loginf << "ImportTrailWidget: test: failed";
}
void ImportTrailWidget::import ()
{
    assert (test_success_);
    assert (file_widget_);

    log_edit_->clear();

    if (!GPSTrailManager::getInstance().hasTrailFilename())
    {
        QMessageBox::warning ( this, "Import Trail", "No Input File selected:\n Please use to the 'Input File' tab to do so.");
        return;
    }

    if (!FormatDefinitionManager::getInstance().hasCurrentFormat())
    {
        QMessageBox::warning ( this, "Test Import Trail", "No Format Definition selected:\n Please use to the 'Format Definition' tab to do so.");
        return;
    }

    if (!FormatDefinitionManager::getInstance().getCurrentFormat()->hasAllColumns())
    {
        QMessageBox::warning ( this, "Test Import Trail", "Format Definition does not have all required columns:\n"
                " Please use to the 'Format Definition' tab to do so.");
        return;
    }

    std::string filename = GPSTrailManager::getInstance().getTrailFilename();
    std::vector <std::string> parts = Utils::String::split(filename, '/');
    assert (parts.size() > 0);
    std::string trail_name = parts.back();

    bool ok;
    QString text = QInputDialog::getText(this, tr("Import GPS Trail"), tr("Enter Format Name (must be unique):"), QLineEdit::Normal,
            trail_name.c_str(), &ok);

    if (ok && !text.isEmpty())
    {
        std::stringstream ss;

        bool ret = GPSTrailManager::getInstance().importTrail(ss, true, text.toStdString(), false);

        log_edit_->setPlainText(ss.str().c_str());

        if (ret)
        {
            loginf << "ImportTrailWidget: import: success";
            test_button_->setDisabled(false);
            import_button_->setDisabled(false);

            emit trailImported();
        }
        else
            loginf << "ImportTrailWidget: import: failed";

    }

}

} /* namespace MFImport */
