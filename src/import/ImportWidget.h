/*
 * ImportWidget.h
 *
 *  Created on: Jun 26, 2014
 *      Author: sk
 */

#ifndef IMPORTWIDGET_H_
#define IMPORTWIDGET_H_

#include <QWidget>
#include <cassert>

namespace MFImport
{

class ImportFileWidget;
class FormatDefinitionManagerWidget;
class ImportTrailWidget;

class ImportWidget : public QWidget
{
    Q_OBJECT

public:
    ImportWidget(QWidget* parent = 0, Qt::WindowFlags f = 0);
    virtual ~ImportWidget();

    ImportTrailWidget *getImportTrailWidget () { assert(import_widget_); return import_widget_;}

protected:
    ImportFileWidget *file_widget_;
    FormatDefinitionManagerWidget *format_widget_;
    ImportTrailWidget *import_widget_;

    void createGUIElements ();
};

} /* namespace MFImport */

#endif /* IMPORTWIDGET_H_ */
