/*
 * ImportFileWidget.cpp
 *
 *  Created on: Jun 26, 2014
 *      Author: sk
 */

#include "ImportFileWidget.h"
#include "Logger.h"
#include "GPSTrailManager.h"

#include <QVBoxLayout>
#include <QLabel>
#include <QPushButton>
#include <QFileDialog>
#include <QTextEdit>
#include <fstream>

namespace MFImport
{

ImportFileWidget::ImportFileWidget(QWidget* parent, Qt::WindowFlags f)
: QWidget (parent, f), filename_label_(0), file_contents_(0)
{
    createGUIElements ();
}

ImportFileWidget::~ImportFileWidget()
{
}

void ImportFileWidget::createGUIElements ()
{
    bool has_filename = GPSTrailManager::getInstance().hasTrailFilename();
    std::string filename = GPSTrailManager::getInstance().getTrailFilename();

    QFont font_bold;
    font_bold.setBold(true);

    QVBoxLayout *layout = new QVBoxLayout ();

    QLabel *file_label = new QLabel ("File Selection");
    file_label->setFont(font_bold);
    layout->addWidget (file_label);

    QHBoxLayout *file_layout = new QHBoxLayout ();

    if (has_filename)
        filename_label_ = new QLabel (filename.c_str());
    else
        filename_label_ = new QLabel ("Please select a input file");

    file_layout->addWidget (filename_label_);

    QPushButton *file_button = new QPushButton ("Select");
    connect (file_button, SIGNAL(clicked()), this, SLOT(selectInputFile()));
    file_layout->addWidget (file_button);
    layout->addLayout (file_layout);

    QLabel *contents_label = new QLabel ("Contents");
    contents_label->setFont(font_bold);
    layout->addWidget (contents_label);

    file_contents_ = new QTextEdit("Please select a input file");
    if (has_filename)
    {
        showFileContents (filename);
        file_contents_->setDisabled(false);
    }
    else
        file_contents_->setDisabled(true);

    file_contents_->setReadOnly(true);
    layout->addWidget (file_contents_);

    setLayout (layout);
}

void ImportFileWidget::selectInputFile ()
{
    assert (filename_label_);
    assert (file_contents_);

    QString fileName = QFileDialog::getOpenFileName(this, tr("Select GPS Trail File"), "", tr("Files (*.*)"));

    if (fileName.size() > 0)
    {
        loginf << "ImportWidget: selectInputFile: input file '" << fileName.toStdString() << "'";
        std::string filename = fileName.toStdString();

        showFileContents (filename);
        GPSTrailManager::getInstance().setTrailFilename(filename);

        filename_label_->setText (fileName);
    }
    else
    {
        filename_label_->setText ("Please select a input file");
        file_contents_->setDisabled(true);
        file_contents_->setText ("Please select a input file");
        GPSTrailManager::getInstance().setTrailFilename("");
    }
}

void ImportFileWidget::showFileContents (std::string filename)
{
    assert (file_contents_);

    std::ifstream infile(filename.c_str());

    std::string line;
    unsigned int cnt=0;
    std::stringstream ss;

    while (std::getline(infile, line) && cnt < 30)
    {
        ss << line << std::endl;
        cnt++;
    }
    if (cnt == 30)
        ss << "...";

    file_contents_->setDisabled(false);
    file_contents_->setText (ss.str().c_str());
}

} /* namespace MFImport */
