/*
 * ImportTrailWidget.h
 *
 *  Created on: Jul 1, 2014
 *      Author: sk
 */

#ifndef IMPORTTRAILWIDGET_H_
#define IMPORTTRAILWIDGET_H_

#include <QWidget>

class QPushButton;
class QTextEdit;

namespace MFImport
{

class ImportFileWidget;

class ImportTrailWidget : public QWidget
{
    Q_OBJECT
public slots:
    void test ();
    void import ();
signals:
    void trailImported ();

public:
    ImportTrailWidget(ImportFileWidget *file_widget, QWidget* parent = 0, Qt::WindowFlags f = 0);
    virtual ~ImportTrailWidget();

protected:
    ImportFileWidget *file_widget_;
    QPushButton *test_button_;
    bool test_success_;
    QPushButton *import_button_;

    QTextEdit *log_edit_;

    void createGUIElements ();
};

} /* namespace MFImport */

#endif /* IMPORTTRAILWIDGET_H_ */
