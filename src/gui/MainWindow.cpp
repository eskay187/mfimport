/*
 * MainWindow.cpp
 *
 *  Created on: Aug 2, 2011
 *      Author: sk
 */

#include <boost/bind.hpp>

#include <QMenuBar>
#include <QMessageBox>
#include <QAction>
#include <QMoveEvent>
#include <QResizeEvent>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QTextEdit>
#include <QPushButton>
#include <QStackedWidget>
#include <QLineEdit>
#include <QGridLayout>

#include "Buffer.h"
#include "MainWindow.h"
#include "Global.h"
#include "Logger.h"
#include "Config.h"
#include "WorkerThreadManager.h"
#include "ConfigurationManager.h"
#include "DBObjectWidget.h"
#include "ATSDB.h"
#include "DBSelectionWidget.h"
#include "DBSchema.h"
#include "DBSchemaManager.h"
#include "DBSchemaWidget.h"
#include "DBConnectionInfo.h"
#include "String.h"
#include "ProjectionManager.h"
#include "MainWidget.h"

using namespace Utils::String;
using namespace std;

MainWindow::MainWindow()
: Configurable ("MainWindow", "MainWindow0"), main_widget_(0), selection_widget_(0), schema_widget_(0),
  db_opened_(false), open_db_(0), start_button_(0), object_widget_ (0),
  center_latitude_edit_ (0), center_longitude_edit_ (0), center_altitude_edit_ (0)
{
    logdbg  << "MainWindow: constructor";

    registerParameter ("pos_x", &pos_x_, 0);
    registerParameter ("pos_y", &pos_y_, 0);
    registerParameter ("width", &width_, 1000);
    registerParameter ("height", &height_, 700);
    registerParameter ("min_width", &min_width_, 1000);
    registerParameter ("min_height", &min_height_, 700);
    registerParameter ("native_menu", &native_menu_, false);

    setMinimumSize(QSize(min_width_, min_height_));
    setGeometry(pos_x_, pos_y_, width_, height_);

    createMenus();

    widget_stack_ = new QStackedWidget ();

    schema_widget_ = new DBSchemaWidget ();
    //connect(this, SIGNAL(openedDatabase()), schema_widget_, SLOT(openedDatabase()));

    createSubConfigurables();

    object_widget_ = new DBObjectWidget ();

    assert (selection_widget_);
    assert (schema_widget_);

    db_config_widget_ = new QWidget ();
    assert (db_config_widget_);
    createDBConfigWidget ();

    widget_stack_->addWidget (db_config_widget_);
    setCentralWidget(widget_stack_);

    widget_stack_->setCurrentIndex (0);

    menuBar()->setNativeMenuBar(native_menu_);

}

MainWindow::~MainWindow()
{
    logdbg  << "MainWindow: destructor";

    // remember: this not called! insert deletes into closeEvent function
}

void MainWindow::openDB()
{
    logdbg  << "MainWindow: open";

    assert (!db_opened_);
    assert (selection_widget_);

    if (selection_widget_->hasDefinedDatabase())
    {
        openDatabase (selection_widget_->getConnectionInfo());
    }

    logdbg  << "MainWindow: open: done";
}

void MainWindow::openDatabase (DBConnectionInfo *info)
{
    loginf  << "MainWindow: openDatabase";

    assert (!db_opened_);

    ATSDB::getInstance().init (info);

    db_opened_=true;
    open_db_->setDisabled (true);

    updateDBInfo();

    assert (start_button_);
    start_button_->setDisabled (false);

    main_widget_ = new MFImport::MainWidget ();
    assert (main_widget_);
    widget_stack_->addWidget (main_widget_);

    setSystemCenter ();
    emit openedDatabase();
}

void MainWindow::start ()
{
    if (db_opened_)
    {
        if (schema_widget_->hasSelectedSchema ())
        {
            assert (DBSchemaManager::getInstance().hasCurrentSchema());

            widget_stack_->setCurrentIndex (1);
            repaint();
            start_button_->setDisabled (true);
        }
    }
}

void MainWindow::createMenus()
{
    logdbg  << "MainWindow: createMenus";

    QAction *exit_action = new QAction(tr("E&xit"), this);
    exit_action->setShortcuts(QKeySequence::Quit);
    exit_action->setStatusTip(tr("Exit the application"));
    connect(exit_action, SIGNAL(triggered()), this, SLOT(close()));

    QMenu *file_menu = menuBar()->addMenu(tr("&File"));
    file_menu->addAction(exit_action);

}

void MainWindow::closeEvent(QCloseEvent *event)
{
    logdbg  << "MainWindow: closeEvent: start";

    ConfigurationManager::getInstance().saveConfiguration();

    if (widget_stack_)
        delete widget_stack_;

    if (db_opened_)
    {
        logdbg  << "MainWindow: closeEvent: database shutdown";

        if (ATSDB::getInstance().getDBOpened ())
            ATSDB::getInstance().shutdown();

        db_opened_=false;
    }
    assert (!ATSDB::getInstance().getDBOpened ());

    WorkerThreadManager::getInstance().shutdown();

    logdbg  << "MainWindow: closeEvent: done";
}

void MainWindow::moveEvent (QMoveEvent *event)
{
    logdbg  << "MainWindow " << instance_id_ << ": moveEvent";
    pos_x_ = event->pos().x();
    pos_y_ = event->pos().y();
}

void MainWindow::resizeEvent (QResizeEvent *event)
{
    logdbg  << "MainWindow " << instance_id_ << ": resizeEvent";
    width_ = event->size().width();
    height_ = event->size().height();
}

void MainWindow::generateSubConfigurable (std::string class_id, std::string instance_id)
{
    if (class_id.compare("DBSelectionWidget") == 0)
    {
        assert (selection_widget_ == 0);
        selection_widget_ = new DBSelectionWidget ("DBSelectionWidget", instance_id, this);
    }
    else
        throw std::runtime_error ("MainWindow: generateSubConfigurable: unknown sub-configurable "+class_id);
}

void MainWindow::checkSubConfigurables ()
{
    if (selection_widget_ == 0)
    {
        generateSubConfigurable ("DBSelectionWidget", "DBSelectionWidget0");
    }
}

void MainWindow::createDBConfigWidget ()
{
    QFont font_bold;
    font_bold.setBold(true);

    QFont font_big;
    font_big.setPointSize(18);

    assert (selection_widget_ != 0);

    QHBoxLayout *sel_layout = new QHBoxLayout ();

    QVBoxLayout *db_type_layout = new QVBoxLayout ();
    db_type_layout->addWidget (selection_widget_);

    db_type_layout->addStretch();

    QLabel *db_info_label = new QLabel("Database information");
    db_type_layout->addWidget (db_info_label);


    database_info_ = new QTextEdit ("Database unopened.");
    database_info_->setReadOnly (true);
    database_info_->setMaximumHeight (info_height_);
    db_type_layout->addWidget (database_info_);


    open_db_ = new QPushButton(tr("Open"));
    open_db_->setFont (font_bold);
    connect(open_db_, SIGNAL( clicked() ), this, SLOT( openDB() ));
    db_type_layout->addWidget(open_db_);

    sel_layout->addLayout (db_type_layout);

    QVBoxLayout *db_schema_layout = new QVBoxLayout ();

    assert (schema_widget_);
    db_schema_layout->addWidget (schema_widget_);

    db_schema_layout->addWidget (object_widget_);

    db_schema_layout->addStretch();

    // center point
    QGridLayout *center_layout = new QGridLayout ();

    QLabel *center_label = new QLabel ("System center point");
    center_label->setFont(font_bold);
    db_schema_layout->addWidget(center_label);

    QLabel *lat_label = new QLabel ("Latitude");
    center_layout->addWidget(lat_label, 0, 0);

    center_latitude_edit_ = new QLineEdit ();
    center_latitude_edit_->setDisabled (true);
    connect (center_latitude_edit_, SIGNAL(returnPressed()), this, SLOT(centerLatitudeChanged()));
    center_layout->addWidget(center_latitude_edit_, 0, 1);

    QLabel *lat_unit_label = new QLabel ("Degrees East");
    center_layout->addWidget(lat_unit_label, 0, 3);

    QLabel *long_label = new QLabel ("Longitude");
    center_layout->addWidget(long_label, 1, 0);

    center_longitude_edit_ = new QLineEdit ();
    center_longitude_edit_->setDisabled (true);
    connect (center_longitude_edit_, SIGNAL(returnPressed()), this, SLOT(centerLongitudeChanged()));
    center_layout->addWidget(center_longitude_edit_, 1, 1);

    QLabel *long_unit_label = new QLabel ("Degrees North");
    center_layout->addWidget(long_unit_label, 1, 3);

    QLabel *alt_label = new QLabel ("Altitude");
    center_layout->addWidget(alt_label, 2, 0);

    center_altitude_edit_ = new QLineEdit ();
    center_altitude_edit_->setDisabled (true);
    connect (center_altitude_edit_, SIGNAL(returnPressed()), this, SLOT(centerAltitudeChanged()));
    center_layout->addWidget(center_altitude_edit_, 2, 1);

    QLabel *alt_unit_label = new QLabel ("Meters above MSL");
    center_layout->addWidget(alt_unit_label, 2, 3);

    db_schema_layout->addLayout (center_layout);

    db_schema_layout->addStretch();

    start_button_ = new QPushButton(tr("Start"));
    start_button_->setFont (font_bold);
    connect(start_button_, SIGNAL( clicked() ), this, SLOT( start() ));
    start_button_->setDisabled (true);
    db_schema_layout->addWidget(start_button_);


    sel_layout->addLayout (db_schema_layout);

    db_config_widget_->setLayout (sel_layout);
}

void MainWindow::keyPressEvent ( QKeyEvent * event )
{
    logdbg  << "MainWindow: keyPressEvent '" << event->text().toStdString() << "'";

    if (event->modifiers()  & Qt::ControlModifier)
    {
        if (event->key() == Qt::Key_U)
        {
            unlockSchemaGui();
        }
    }
}

void MainWindow::centerLatitudeChanged ()
{
    std::string value = center_latitude_edit_->text().toStdString();
    double val = doubleFromString (value);
    ProjectionManager::getInstance().setCenterLatitude(val);
}
void MainWindow::centerLongitudeChanged ()
{
    std::string value = center_longitude_edit_->text().toStdString();
    double val = doubleFromString (value);
    ProjectionManager::getInstance().setCenterLongitude(val);
}
void MainWindow::centerAltitudeChanged ()
{
    std::string value = center_altitude_edit_->text().toStdString();
    double val = doubleFromString (value);
    ProjectionManager::getInstance().setCenterAltitude(val);
}

void MainWindow::unlockSchemaGui()
{
    loginf  << "MainWindow: unlockDBGui";

    if (schema_widget_)
        schema_widget_->unlock();

    if (object_widget_)
        object_widget_->unlock();
}

void MainWindow::setSystemCenter ()
{
    assert (center_latitude_edit_);
    assert (center_longitude_edit_);
    assert (center_altitude_edit_);

    center_latitude_edit_->setText(doubleToString(ProjectionManager::getInstance().getCenterLatitude()).c_str());
    center_latitude_edit_->setDisabled(false);
    center_longitude_edit_->setText(doubleToString(ProjectionManager::getInstance().getCenterLongitude()).c_str());
    center_longitude_edit_->setDisabled(false);
    center_altitude_edit_->setText(doubleToString(ProjectionManager::getInstance().getCenterAltitude()).c_str());
    center_altitude_edit_->setDisabled(false);

    logdbg << "MainWindow: setSystemCenter: lat " << center_latitude_edit_->text().toStdString() << " long "
            << center_longitude_edit_->text().toStdString();
}

void MainWindow::setDBType (std::string value)
{
    assert (selection_widget_);
    selection_widget_->setDBType(value);
}
void MainWindow::setDBFilename (std::string value)
{
    assert (selection_widget_);
    selection_widget_->setDBFilename(value);
}


void MainWindow::setDBServer (std::string value)
{
    assert (selection_widget_);
    selection_widget_->setDBServer(value);
}
void MainWindow::setDBName (std::string value)
{
    assert (selection_widget_);
    selection_widget_->setDBName(value);
}
void MainWindow::setDBPort (std::string value)
{
    assert (selection_widget_);
    selection_widget_->setDBPort(value);

}
void MainWindow::setDBUser (std::string value)
{
    assert (selection_widget_);
    selection_widget_->setDBUser(value);

}
void MainWindow::setDBPassword (std::string value)
{
    assert (selection_widget_);
    selection_widget_->setDBPassword(value);

}

void MainWindow::setDBNoPassword ()
{
    assert (selection_widget_);
    selection_widget_->setDBNoPassword();
}
void MainWindow::setDBSchema (std::string value)
{
    assert (schema_widget_);
    schema_widget_->setSchema(value);
}


void MainWindow::triggerAutoStart ()
{
    loginf  << "MainWindow: triggerAutoStart";
    openDB ();
    if (db_opened_)
        start ();
    else
    {
        logerr  << "MainWindow: triggerAutoStart: db open failed";
    }
}

void MainWindow::updateDBInfo ()
{
    assert (db_opened_);

    std::stringstream ss;

    ss << ATSDB::getInstance().getDBInfo()->getIdString() << endl << endl;

    Buffer *tables = ATSDB::getInstance().getTableList ();

    if (tables->getFirstWrite())
    {
        ss << "No tables found." << endl;
        database_info_->setText (ss.str().c_str());
        delete tables;
        return;
    }

    tables->setIndex(0);
    std::string table_name;

    ss << "Table List:";

    for (unsigned int cnt=0; cnt < tables->getSize(); cnt++)
    {
        if (cnt != 0)
            tables->incrementIndex();

        table_name = *(std::string *) tables->get(0);

        ss << " " << table_name;
    }

    delete tables;

    assert (database_info_);

    database_info_->setText (ss.str().c_str());

}

