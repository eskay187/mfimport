/*
 * MainWindow.h
 *
 *  Created on: Aug 2, 2011
 *      Author: sk
 */

#ifndef MAINWINDOW_H_
#define MAINWINDOW_H_

#include <QMainWindow>
#include "Configurable.h"
#include "JobOrderer.h"

class Job;
class QTextEdit;
class QPushButton;
class DBConnectionInfo;
class DBObjectWidget;
class DBSelectionWidget;
class DBSchemaWidget;
class QStackedWidget;
class QHBoxLayout;
class QLineEdit;
//class QProgressDialog;

namespace Check
{
class CheckManagerWidget;
}

namespace MFImport
{
    class MainWidget;
}
/**
 * @brief Main window which embeds all other components
 *
 * When started, allows management of database connection and schema. When database is opened,
 * a stack widget is used to display the main widget with further components.
 *
 * Also handles shutdown behavior using the closeEvent() function.
 */
class MainWindow : public QMainWindow, public Configurable
{
    Q_OBJECT

signals:
    /// @brief Emitted when database was opened
    void openedDatabase();

private slots:
    /// @brief Opens the database
    void openDB();
    /// @brief If database is open, switch to ManagementWidget
    void start ();

    /// @brief Handles key press events
    void keyPressEvent ( QKeyEvent * event );

    /// @brief Sets the system center latitude
    void centerLatitudeChanged ();
    /// @brief Sets the system center longitude
    void centerLongitudeChanged ();
    /// @brief Sets the system center altitude
    void centerAltitudeChanged ();

public:
    /// @brief Constructor
    MainWindow();
    /// @brief Destructor
    virtual ~MainWindow();

    virtual void generateSubConfigurable (std::string class_id, std::string instance_id);

    /// @brief Sets database type
    void setDBType (std::string value);
    /// @brief Sets database filename
    void setDBFilename (std::string value);
    /// @brief Sets database server
    void setDBServer (std::string value);
    /// @brief Sets database name
    void setDBName (std::string value);
    /// @brief Sets database port number
    void setDBPort (std::string value);
    /// @brief Sets database username
    void setDBUser (std::string value);
    /// @brief Sets database password
    void setDBPassword (std::string value);
    /// @brief Sets database usage without password
    void setDBNoPassword ();
    /// @brief Sets database schema
    void setDBSchema (std::string value);
    /// @brief Triggers a autostart without user interaction (from console parameters)
    void triggerAutoStart ();

protected:
    /// Widget stack for startup to usage switch
    QStackedWidget *widget_stack_;
    /// Database configuration widget
    QWidget *db_config_widget_;
    /// Central widget
    MFImport::MainWidget *main_widget_;

    /// Contains database type and parameter elements
    DBSelectionWidget *selection_widget_;
    /// Contains database schema configuration elements
    DBSchemaWidget *schema_widget_;
    /// Contains DBObject configuration elements
    DBObjectWidget *object_widget_;

    /// System center latitude edit field
    QLineEdit *center_latitude_edit_;
    /// System center longitude edit field
    QLineEdit *center_longitude_edit_;
    /// System center altitude edit field
    QLineEdit *center_altitude_edit_;

    static const unsigned int info_height_=200;

    QPushButton *open_db_;
    QTextEdit *database_info_;

    QPushButton *start_button_;

    bool db_opened_;

    unsigned int pos_x_;
    unsigned int pos_y_;
    unsigned int width_;
    unsigned int height_;
    unsigned int min_width_;
    unsigned int min_height_;
    bool native_menu_;

    /// @brief Creates File menu
    void createMenus();
    /// @brief Creates database configuration widget
    void createDBConfigWidget ();
    /// @brief Opens the database
    void openDatabase (DBConnectionInfo *info);

    /// @brief Called when application closes
    void closeEvent(QCloseEvent *event);
    /// @brief Called when window is moved, for position persistence
    virtual void moveEvent (QMoveEvent *event);
    /// @brief Called when window is resized, for size persistence
    virtual void resizeEvent (QResizeEvent *event);

    /// @brief Unlocks database schema edit elements
    void unlockSchemaGui();
    /// @brief Shows database information after opening
    void updateDBInfo ();

    virtual void checkSubConfigurables ();

    /// @brief Sets the system center from DrawEngine
    void setSystemCenter ();
};
#endif /* MAINWINDOW_H_ */
