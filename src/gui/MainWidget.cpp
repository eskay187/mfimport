/*
 * MainWidget.cpp
 *
 *  Created on: Jun 25, 2014
 *      Author: sk
 */

#include "MainWidget.h"
#include "GPSTrailManagerWidget.h"
#include "MeasurementFlightManagerWidget.h"
#include "InsertManagerWidget.h"

#include <QVBoxLayout>

namespace MFImport
{

MainWidget::MainWidget (QWidget* parent, Qt::WindowFlags f)
: QWidget (parent, f), trail_widget_(0), flight_widget_(0), insert_widget_(0)
{
    createGUIElements ();
}

MainWidget::~MainWidget()
{
    if (trail_widget_)
        delete trail_widget_;
    trail_widget_=0;

    if (flight_widget_)
        delete flight_widget_;
    flight_widget_=0;

    if (insert_widget_)
        delete insert_widget_;
    insert_widget_=0;
}

void MainWidget::createGUIElements ()
{
    QVBoxLayout *layout = new QVBoxLayout ();

    trail_widget_ = new GPSTrailManagerWidget ();
    layout->addWidget (trail_widget_);

    flight_widget_ = new MeasurementFlightManagerWidget ();
    layout->addWidget (flight_widget_);
    connect (trail_widget_, SIGNAL (convert(GPSTrail*)), flight_widget_, SLOT(convert(GPSTrail*)));

    insert_widget_ = new InsertManagerWidget ();
    layout->addWidget (insert_widget_);
    connect (flight_widget_, SIGNAL (insertFlight(std::string)), insert_widget_, SLOT(updateCurrentMeasurementFlight(std::string)));

    setLayout (layout);
}

} /* namespace MFImport */
