/*
 * MainWidget.h
 *
 *  Created on: Jun 25, 2014
 *      Author: sk
 */

#ifndef MAINWIDGET_H_
#define MAINWIDGET_H_

#include "Configurable.h"

#include <QWidget>

namespace MFImport
{

class GPSTrailManagerWidget;
class MeasurementFlightManagerWidget;
class InsertManagerWidget;

class MainWidget : public QWidget, public Configurable
{
public:
    MainWidget(QWidget* parent = 0, Qt::WindowFlags f = 0);
    virtual ~MainWidget();

protected:
    GPSTrailManagerWidget *trail_widget_;
    MeasurementFlightManagerWidget *flight_widget_;
    InsertManagerWidget *insert_widget_;

    void createGUIElements ();
};

} /* namespace MFImport */

#endif /* MAINWIDGET_H_ */
