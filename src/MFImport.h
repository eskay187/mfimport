/*
 * MFImport.h
 *
 *  Created on: Jul 30, 2009
 *      Author: sk
 */

#ifndef PALANTIR_H_
#define PALANTIR_H_

#include <QApplication>

namespace MFImport
{

class MFImport : public QApplication
{
public:
  MFImport(int& argc, char ** argv);

  virtual ~MFImport() { }

  // reimplemented from QApplication so we can throw exceptions in slots
  virtual bool notify(QObject * receiver, QEvent * event);

};
}

#endif /* PALANTIR_H_ */
