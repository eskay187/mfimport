/*
 * GPSTrailManager.cpp
 *
 *  Created on: Jun 25, 2014
 *      Author: sk
 */

#include "GPSTrailManager.h"
#include "GPSTrail.h"
#include "FormatDefinitionManager.h"
#include "FormatDefinition.h"
#include <fstream>

namespace MFImport
{

GPSTrailManager::GPSTrailManager()
 : Singleton(), Configurable ("GPSTrailManager", "GPSTrailManager0", 0, "conf/config_gpstrail.xml")
{
    registerParameter ("trail_filename", &trail_filename_, "");

    createSubConfigurables();
}

GPSTrailManager::~GPSTrailManager()
{
    std::map <std::string, GPSTrail*>::iterator it;

    for (it = trails_.begin(); it != trails_.end(); it++)
        delete it->second;
    trails_.clear();
}

void GPSTrailManager::generateSubConfigurable (std::string class_id, std::string instance_id)
{
    if (class_id == "GPSTrail")
    {
        GPSTrail *trail = new GPSTrail (class_id, instance_id, this);
        if (trails_.find(trail->getName()) != trails_.end())
        {
            logwrn << "GPSTrailManager: generateSubConfigurable: trail with name '" << trail->getName() << "' already exists, removing";
            delete trail;
            return;
        }

        trails_[trail->getName()] = trail;
    }
    else
        throw std::runtime_error ("GPSTrailManager: generateSubConfigurable: unknown class_id "+class_id);
}

bool GPSTrailManager::importTrail (std::stringstream &ss, bool create_trail, std::string trail_name, bool log_flag)
{
    loginf << "GPSTrailManager: importTrail: create trail " << create_trail << " name '" << trail_name << "' log " << log_flag;

    if (create_trail)
    {
        assert (trail_name.size() != 0);
        assert (trails_.find(trail_name) == trails_.end());
    }
    else
        assert (trail_name.size() == 0);

    if (create_trail)
        ss << "Starting import:" << std::endl;
    else
        ss << "Starting test:" << std::endl;

    ss << "Using file '" << trail_filename_ << "'" << std::endl;

    if (!FormatDefinitionManager::getInstance().hasCurrentFormat ())
    {
        ss << "Failed: No current format definition selected" << std::endl;
        return false;
    }

    GPSTrail *trail=0;
    if (create_trail)
    {
        Configuration &config = addNewSubConfiguration("GPSTrail");
        config.addParameterString ("name", trail_name);
        generateSubConfigurable(config.getClassId(), config.getInstanceId());
        assert (trails_.find(trail_name) != trails_.end());
        trail = trails_.at(trail_name);
        ss << "Created GPS Trail with name " << trail_name;
        assert (trail->getNumUpdates() == 0);
    }

    FormatDefinition *fo_def = FormatDefinitionManager::getInstance().getCurrentFormat();
    ss << "Using format definition " << fo_def->getName() << std::endl;

    std::ifstream infile(trail_filename_.c_str());

    if (!infile.is_open())
    {
         ss << "Failed: Could not open file " << trail_filename_ << std::endl;

         if (trail)
         {
             deleteTrail (trail_name);
             trail = 0;
         }

         return false;
    }
    ss << "Opening file successful" << std::endl;

    std::string line;
    unsigned int cnt=0;
    bool ret;

    while (std::getline(infile, line))
    {
        if (!create_trail && cnt > 999)
            break;

        if (log_flag)
            ss << "Line " << cnt << ": Parsing '" << line << "'" << std::endl;

        //loginf << "GPSTrailManager: importTrail: line " << cnt;

        ret = fo_def->parse(cnt, line, trail, ss, log_flag);

        if (!ret)
        {
            ss << "Line " << cnt << ": Parsing failed" << std::endl;
            break;
        }
        cnt++;
    }

    if (!ret)
    {
        if (trail)
        {
            deleteTrail (trail_name);
            trail = 0;
        }
        return false;
    }

    ss << cnt << " lines successfully parsed" << std::endl;

    if (trail)
    {
        ss << "GPS trail " << trail_name << " created with " << trail->getNumUpdates() << std::endl;
    }

    return true;
}

void GPSTrailManager::deleteTrail (std::string name)
{
    assert (hasTrail(name));
    std::map <std::string, GPSTrail*>::iterator it;
    it = trails_.find(name);
    GPSTrail *trail = it->second;

    trails_.erase(it);
    delete trail;
    return;
}

void GPSTrailManager::updateGPSTrails ()
{
    std::map <std::string, GPSTrail*> old = trails_;
    std::map <std::string, GPSTrail*>::iterator it;

    trails_.clear();

    for (it = old.begin(); it != old.end(); it++)
    {
        assert (trails_.find(it->second->getName()) == trails_.end());
        trails_[it->second->getName()] = it->second;
    }
}

} /* namespace MFImport */
