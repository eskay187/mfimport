/*
 * GPSTrail.cpp
 *
 *  Created on: Jun 24, 2014
 *      Author: sk
 */

#include "GPSTrail.h"
#include "GPSUpdate.h"
#include "String.h"

using namespace Utils::String;

namespace MFImport
{
GPSTrail::GPSTrail(std::string class_id, std::string instance_id, Configurable *parent)
: Configurable (class_id, instance_id, parent), tod_min_(0), tod_max_(0), latitude_min_ (0), latitude_max_ (0),
longitude_min_ (0), longitude_max_ (0), altitude_min_ (0), altitude_max_ (0)
{
    registerParameter ("name", &name_, "");
    assert (name_.size() > 0);

    createSubConfigurables();
}

GPSTrail::~GPSTrail()
{
    std::map <double, GPSUpdate*>::iterator it;

    for (it = updates_.begin(); it != updates_.end(); it++)
        delete it->second;
    updates_.clear();
}

void GPSTrail::generateSubConfigurable (std::string class_id, std::string instance_id)
{
    if (class_id == "GPSUpdate")
    {
        GPSUpdate *update = new GPSUpdate (class_id, instance_id, this);

        if (updates_.find(update->getTod()) != updates_.end())
        {
            logwrn << "GPSTrail: generateSubConfigurable: removing new update with same tod as another " << timeStringFromDouble(update->getTod());
            delete update;
            return;
        }

        if (updates_.size() == 0)
        {
            tod_min_ = update->getTod();
            tod_max_ = update->getTod();

            latitude_min_ = update->getLatitude();
            latitude_max_ = update->getLatitude();
            longitude_min_ = update->getLongitude();
            longitude_max_ = update->getLongitude();
            altitude_min_ = update->getAltitude();
            altitude_max_ = update->getAltitude();
        }
        else
        {
            if (update->getTod() < tod_min_)
                tod_min_ = update->getTod();

            if (update->getTod() > tod_max_)
                tod_max_ = update->getTod();

            if (update->getLatitude() < latitude_min_)
                latitude_min_ = update->getLatitude();

            if (update->getLatitude() > latitude_max_)
                latitude_max_ = update->getLatitude();

            if (update->getLongitude() < longitude_min_)
                longitude_min_ = update->getLongitude();

            if (update->getLongitude() > longitude_max_)
                longitude_max_ = update->getLongitude();

            if (update->getAltitude() < altitude_min_)
                altitude_min_ = update->getAltitude();

            if (update->getAltitude() > altitude_max_)
                altitude_max_ = update->getAltitude();
        }

        updates_[update->getTod()] = update;
    }
    else
        throw std::runtime_error ("GPSTrail: generateSubConfigurable: unknown class_id "+class_id);
}

void GPSTrail::addUpdate (double tod, double latitude, double longitude, double altitude)
{
    if (updates_.find(tod) != updates_.end())
        throw std::runtime_error ("GPSTrail: addUpdate: update with time " + doubleToString(tod) + " already exists, skipping");

    Configuration &config = addNewSubConfiguration("GPSUpdate", "GPSUpdate"+Utils::String::intToString(updates_.size()));
    config.addParameterDouble("tod", tod);
    config.addParameterDouble("latitude", latitude);
    config.addParameterDouble("longitude", longitude);
    config.addParameterDouble("altitude", altitude);
    generateSubConfigurable (config.getClassId(), config.getInstanceId());

    //loginf << "GPSTrail: addUpdate: done, size " << updates_.size();
}
}
