/*
 * GPSTrailManagerWidget.cpp
 *
 *  Created on: Jun 25, 2014
 *      Author: sk
 */

#include "GPSTrailManagerWidget.h"
#include "GPSTrailManager.h"
#include "GPSTrail.h"
#include "ImportWidget.h"
#include "ImportTrailWidget.h"
#include "String.h"

#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QPushButton>
#include <QTableWidget>
#include <QTableWidgetItem>
#include <QLineEdit>
#include <QMessageBox>

using namespace Utils::String;

namespace MFImport
{

GPSTrailManagerWidget::GPSTrailManagerWidget(QWidget* parent, Qt::WindowFlags f)
: QWidget (parent, f), table_(0), import_widget_(0)
{
    createGUIElements ();
    updateTrailTable ();
}

GPSTrailManagerWidget::~GPSTrailManagerWidget()
{
    if (import_widget_)
    {
        delete import_widget_;
        import_widget_=0;
    }
}

void GPSTrailManagerWidget::createGUIElements ()
{
    QFont font_bold;
    font_bold.setBold(true);

    QFont font_big;
    font_big.setPointSize(18);

    QVBoxLayout *hlayout = new QVBoxLayout();

    QLabel *label = new QLabel ("GPS Trails");
    label->setFont (font_big);
    hlayout->addWidget (label);

    // GPS trails table

    table_ = new QTableWidget ();
    table_->setSelectionBehavior(QAbstractItemView::SelectRows);
    table_->setSelectionMode(QAbstractItemView::SingleSelection);

    QStringList header_list;
    header_list.append (tr("Name"));
    header_list.append (tr("Time"));
    header_list.append (tr("Duration"));
    header_list.append (tr("Latitude"));
    header_list.append (tr("Longitude"));
    header_list.append (tr("Altitude"));
    header_list.append (tr("Delete"));
    header_list.append (tr("Convert"));
    table_->setColumnCount(header_list.size());
    table_->setHorizontalHeaderLabels (header_list);

    hlayout->addWidget (table_);

    //buttons
    QHBoxLayout *blayout = new QHBoxLayout ();

    blayout->addStretch();

    QPushButton *import = new QPushButton ("Import");
    import->setToolTip("Import new GPS Trail");
    connect (import, SIGNAL(clicked ()), this, SLOT (import()));
    blayout->addWidget (import);

    hlayout->addLayout (blayout);

    setLayout(hlayout);
}

void GPSTrailManagerWidget::updateTrailTable ()
{
    assert (table_);
    table_->clearContents();

    name_items_.clear();
    delete_buttons_.clear();
    convert_buttons_.clear();

    QPixmap* pixmapdel = new QPixmap("icons/close_icon.png");
    QPixmap* pixmapconvert = new QPixmap("icons/trail.png");

    const std::map <std::string, GPSTrail*> &trails = GPSTrailManager::getInstance().getTrails ();
    std::map <std::string, GPSTrail*>::const_iterator it;

    table_->setRowCount(trails.size());

    unsigned int row = 0;
    QTableWidgetItem *newItem=0;
    GPSTrail *trail;
    for (it = trails.begin(); it != trails.end(); it++)
    {
        trail = it->second;

        QLineEdit *name_edit = new QLineEdit(trail->getName().c_str());
        name_items_[name_edit] = trail->getName();
        name_edit->setMinimumWidth(300);
        table_->setCellWidget(row, 0, name_edit);
        connect (name_edit, SIGNAL(returnPressed()), this, SLOT(nameChanged()));

        std::string tmp = timeStringFromDouble(trail->getTODMinimum()) + " - " + timeStringFromDouble(trail->getTODMaximum());
        newItem = new QTableWidgetItem(tmp.c_str());
        newItem->setFlags(Qt::ItemIsEnabled);
        table_->setItem(row, 1, newItem);

        newItem = new QTableWidgetItem(timeStringFromDouble(trail->getTODDuration()).c_str());
        newItem->setFlags(Qt::ItemIsEnabled);
        table_->setItem(row, 2, newItem);

        tmp = doubleToStringPrecision(trail->getLatitudeMin(),2) + " - " + doubleToStringPrecision(trail->getLatitudeMax(),2);
        newItem = new QTableWidgetItem(tmp.c_str());
        newItem->setFlags(Qt::ItemIsEnabled);
        table_->setItem(row, 3, newItem);

        tmp = doubleToStringPrecision(trail->getLongitudeMin(),2) + " - " + doubleToStringPrecision(trail->getLongitudeMax(),2);
        newItem = new QTableWidgetItem(tmp.c_str());
        newItem->setFlags(Qt::ItemIsEnabled);
        table_->setItem(row, 4, newItem);

        tmp = doubleToStringPrecision(trail->getAltitudeMin(),2) + " - " + doubleToStringPrecision(trail->getAltitudeMax(),2);
        newItem = new QTableWidgetItem(tmp.c_str());
        newItem->setFlags(Qt::ItemIsEnabled);
        table_->setItem(row, 5, newItem);

        QPushButton *del = new QPushButton ();
        del->setIcon(QIcon(*pixmapdel));
        del->setFlat(true);
        del->setToolTip(tr("Delete trail"));
        table_->setCellWidget(row, 6, del);
        delete_buttons_[del] = it->second->getName();
        connect (del, SIGNAL(clicked()), this, SLOT(deleteTrail()));

        QPushButton *convert = new QPushButton ();
        convert->setIcon(QIcon(*pixmapconvert));
        convert->setFlat(true);
        convert->setToolTip(tr("Convert trail"));
        table_->setCellWidget(row, 7, convert);
        convert_buttons_[convert] = it->second->getName();
        connect (convert, SIGNAL(clicked()), this, SLOT(convertTrail()));

        row++;
    }

    table_->resizeColumnsToContents();
}

void GPSTrailManagerWidget::nameChanged ()
{
    QLineEdit *edit = dynamic_cast<QLineEdit*> (QObject::sender());
    assert (edit);
    assert (name_items_.find (edit) != name_items_.end());

    std::string name = name_items_[edit];
    std::string new_name = edit->text().toStdString();
    assert (GPSTrailManager::getInstance().hasTrail(name));

    if (GPSTrailManager::getInstance().hasTrail(new_name))
    {
        QMessageBox::warning ( this, "GPS Trail Name Change", "GPS Trail with name "+edit->text()+" already exists");
        edit->setText (name.c_str());
        return;
    }

    GPSTrail *trail = GPSTrailManager::getInstance().getTrail(name);

    trail->setName(new_name);

    GPSTrailManager::getInstance().updateGPSTrails();

    loginf << "GPSTrailManagerWidget: nameChanged: trail name changed to " << new_name;

    updateTrailTable ();
}

void GPSTrailManagerWidget::import ()
{
    if (!import_widget_)
    {
        import_widget_ = new ImportWidget ();
        connect ( import_widget_->getImportTrailWidget(), SIGNAL(trailImported()), this, SLOT(trailImported()) );
    }

    import_widget_->show();
}

void GPSTrailManagerWidget::deleteTrail ()
{
    QPushButton *button = dynamic_cast<QPushButton*> (QObject::sender());
    assert (button);
    assert (delete_buttons_.find(button) != delete_buttons_.end());

    std::string name = delete_buttons_[button];
    assert (GPSTrailManager::getInstance().hasTrail(name));
    GPSTrailManager::getInstance().deleteTrail(name);

    loginf << "GPSTrailManagerWidget: deleteTrail: deleted trail " << name;

    updateTrailTable();
}

void GPSTrailManagerWidget::convertTrail ()
{
    QPushButton *button = dynamic_cast<QPushButton*> (QObject::sender());
    assert (button);
    assert (convert_buttons_.find(button) != convert_buttons_.end());

    std::string name = convert_buttons_[button];
    assert (GPSTrailManager::getInstance().hasTrail(name));

    loginf << "GPSTrailManagerWidget: convertTrail: converting trail " << name;
    emit convert(GPSTrailManager::getInstance().getTrail(name));
}

void GPSTrailManagerWidget::trailImported ()
{
    updateTrailTable();
}

} /* namespace MFImport */
