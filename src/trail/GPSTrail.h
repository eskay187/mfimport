/*
 * GPSTrail.h
 *
 *  Created on: Jun 24, 2014
 *      Author: sk
 */

#ifndef GPSTRAIL_H_
#define GPSTRAIL_H_

#include "Configurable.h"
#include <map>
#include <set>

namespace MFImport
{
class GPSUpdate;

class GPSTrail : public Configurable
{
public:
    GPSTrail(std::string class_id, std::string instance_id, Configurable *parent);
    virtual ~GPSTrail();

    virtual void generateSubConfigurable (std::string class_id, std::string instance_id);

    const std::map <double, GPSUpdate*> &getUpdates () { return updates_; }
    const std::string &getName () { return name_; }
    void setName (std::string value) { name_=value; }

    void addUpdate (double tod, double latitude, double longitude, double altitude); // only for new trail
    bool hasUpdates () { return updates_.size() != 0; }
    unsigned int getNumUpdates () { return updates_.size(); }

    double getTODMinimum () { assert (hasUpdates()); return tod_min_; }
    double getTODMaximum () { assert (hasUpdates()); return tod_max_; }
    double getTODDuration () { assert (hasUpdates()); assert (tod_min_<=tod_max_); return tod_max_-tod_min_; }

    double getAltitudeMax() const { return altitude_max_; }
    double getAltitudeMin() const { return altitude_min_; }
    double getLatitudeMax() const { return latitude_max_; }
    double getLatitudeMin() const { return latitude_min_; }
    double getLongitudeMax() const { return longitude_max_; }
    double getLongitudeMin() const { return longitude_min_; }

protected:
    std::string name_;
    std::map <double, GPSUpdate*> updates_;

    double tod_min_;
    double tod_max_;
    double latitude_min_;
    double latitude_max_;
    double longitude_min_;
    double longitude_max_;
    double altitude_min_;
    double altitude_max_;

    virtual void checkSubConfigurables () {}
};
}

#endif /* GPSTRAIL_H_ */
