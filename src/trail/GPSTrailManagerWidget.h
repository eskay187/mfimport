/*
 * GPSTrailManagerWidget.h
 *
 *  Created on: Jun 25, 2014
 *      Author: sk
 */

#ifndef GPSTRAILMANAGERWIDGET_H_
#define GPSTRAILMANAGERWIDGET_H_

#include <QWidget>
#include <map>

class QLineEdit;
class QTableWidget;
class QTableWidgetItem;
class QPushButton;

namespace MFImport
{
class GPSTrail;
class ImportWidget;

class GPSTrailManagerWidget : public QWidget
{
    Q_OBJECT
signals:
    void convert (GPSTrail *trail);

public slots:
    void nameChanged ();
    void import ();
    void deleteTrail ();
    void convertTrail ();
    void trailImported ();

public:
    GPSTrailManagerWidget(QWidget* parent = 0, Qt::WindowFlags f = 0);
    virtual ~GPSTrailManagerWidget();

protected:
    QTableWidget *table_;

    std::map <QLineEdit *, std::string> name_items_;
    std::map <QPushButton *, std::string> delete_buttons_;
    std::map <QPushButton *, std::string> convert_buttons_;

    ImportWidget *import_widget_;

    void createGUIElements ();

    void updateTrailTable ();
};

} /* namespace MFImport */

#endif /* GPSTRAILMANAGERWIDGET_H_ */
