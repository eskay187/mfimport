/*
 * GPSTrailManager.h
 *
 *  Created on: Jun 25, 2014
 *      Author: sk
 */

#ifndef GPSTRAILMANAGER_H_
#define GPSTRAILMANAGER_H_

#include "Singleton.h"
#include "Configurable.h"

namespace MFImport
{
class GPSTrail;


class GPSTrailManager : public Singleton, public Configurable
{
public:
    static GPSTrailManager& getInstance()
    {
      static GPSTrailManager instance;
      return instance;
    }
    virtual ~GPSTrailManager();

    virtual void generateSubConfigurable (std::string class_id, std::string instance_id);

    const std::map <std::string, GPSTrail*> &getTrails () { return trails_; }
    bool hasTrail (std::string name) { return trails_.find(name) != trails_.end(); }
    GPSTrail *getTrail (std::string name) { assert (hasTrail(name)); return trails_.at(name);}
    void deleteTrail (std::string name);

    bool hasTrailFilename () { return trail_filename_.size() != 0; }
    const std::string getTrailFilename () { return trail_filename_; }
    void setTrailFilename (std::string value) { trail_filename_=value; }

    bool importTrail (std::stringstream &ss, bool create_trail, std::string trail_name, bool log_flag);

    void updateGPSTrails ();

protected:
    std::string trail_filename_;
    std::map <std::string, GPSTrail*> trails_;

    GPSTrailManager();
    virtual void checkSubConfigurables () {}

};

} /* namespace MFImport */

#endif /* GPSTRAILMANAGER_H_ */
