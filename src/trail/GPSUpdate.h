/*
 * GPSUpdate.h
 *
 *  Created on: Jun 24, 2014
 *      Author: sk
 */

#ifndef GPSUPDATE_H_
#define GPSUPDATE_H_

#include "Configurable.h"

namespace MFImport
{
class GPSUpdate : public Configurable
{
public:
    GPSUpdate (std::string class_id, std::string instance_id, Configurable *parent)
    : Configurable (class_id, instance_id, parent)
    {
        registerParameter ("tod", &tod_, 0.0);
        registerParameter ("latitude", &latitude_, 0.0);
        registerParameter ("longitude", &longitude_, 0.0);
        registerParameter ("altitude", &altitude_, 0.0);
    }
    ~GPSUpdate () {}

    double getAltitude() const
    {
        return altitude_;
    }

    void setAltitude(double altitude)
    {
        altitude_ = altitude;
    }

    double getLatitude() const
    {
        return latitude_;
    }

    void setLatitude(double latitude)
    {
        latitude_ = latitude;
    }

    double getLongitude() const
    {
        return longitude_;
    }

    void setLongitude(double longitude)
    {
        longitude_ = longitude;
    }

    double getTod() const
    {
        return tod_;
    }

    void setTod(double tod)
    {
        tod_ = tod;
    }

protected:
    double tod_;
    double latitude_;
    double longitude_;
    double altitude_;
};
}



#endif /* GPSUPDATE_H_ */
